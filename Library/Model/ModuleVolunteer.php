<?php
namespace Airhead\Library\Model;

class ModuleVolunteer
{
    /**
     * @var string
     */
    private $email;

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var object
     */
    private $preferredDays;

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = (string)$email;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = (int)$id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = (string)$name;
    }

    /**
     * @param bool $asString
     * @return object|string
     */
    public function getPreferredDays($asString = false)
    {
        if ($asString === true) {
            $return = [];
            foreach ($this->preferredDays as $day => $bool) {
                if ($bool === true) {
                    $return[] = $day;
                }
            }

            return implode(', ', $return);
        }

        return $this->preferredDays;
    }

    /**
     * @param object $preferredDays
     */
    public function setPreferredDays($preferredDays)
    {
        $this->preferredDays = (object)$preferredDays;
    }
}