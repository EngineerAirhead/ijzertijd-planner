<?php
namespace Airhead\Library\Model;

class ModuleAgenda
{
    /**
     * @var string
     */
    private $class;

    /**
     * @var bool
     */
    private $confirmed;

    /**
     * @var bool
     */
    private $consume;

    /**
     * @var string
     */
    private $date;

    /**
     * @var string
     */
    private $email;

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $information;

    /**
     * @var string
     */
    private $name;

    /**
     * @var int
     */
    private $numAdults;

    /**
     * @var int
     */
    private $numKids;

    /**
     * @var string
     */
    private $phone;

    /**
     * @var string
     */
    private $school;

    /**
     * @var string
     */
    private $schoolAddress;

    /**
     * @var string
     */
    private $schoolEmail;

    /**
     * @var string
     */
    private $schoolPhone;

    /**
     * @var string
     */
    private $timeArrive;

    /**
     * @var string
     */
    private $timeDepart;

    /**
     * @var ModuleVolunteer[]
     */
    private $volunteers;

    /**
     * @return string
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * @param string $class
     */
    public function setClass($class)
    {
        $this->class = (string)$class;
    }

    /**
     * @return boolean
     */
    public function isConfirmed()
    {
        return $this->confirmed;
    }

    /**
     * @param boolean $confirmed
     */
    public function setConfirmed($confirmed)
    {
        $this->confirmed = (bool)$confirmed;
    }

    /**
     * @return bool
     */
    public function isConsume()
    {
        return $this->consume;
    }

    /**
     * @param bool $consume
     */
    public function setConsume($consume)
    {
        $this->consume = (bool)$consume;
    }

    /**
     * @return string
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param string $date
     */
    public function setDate($date)
    {
        $this->date = (string)$date;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = (string)$email;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = (int)$id;
    }

    /**
     * @return string
     */
    public function getInformation()
    {
        return $this->information;
    }

    /**
     * @param string $information
     */
    public function setInformation($information)
    {
        $this->information = (string)$information;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = (string)$name;
    }

    /**
     * @return int
     */
    public function getNumAdults()
    {
        return $this->numAdults;
    }

    /**
     * @param int $numAdults
     */
    public function setNumAdults($numAdults)
    {
        $this->numAdults = (int)$numAdults;
    }

    /**
     * @return int
     */
    public function getNumKids()
    {
        return $this->numKids;
    }

    /**
     * @param int $numKids
     */
    public function setNumKids($numKids)
    {
        $this->numKids = (int)$numKids;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = (string)$phone;
    }

    /**
     * @return string
     */
    public function getSchool()
    {
        return $this->school;
    }

    /**
     * @param string $school
     */
    public function setSchool($school)
    {
        $this->school = (string)$school;
    }

    /**
     * @return string
     */
    public function getSchoolAddress()
    {
        return $this->schoolAddress;
    }

    /**
     * @param string $schoolAddress
     */
    public function setSchoolAddress($schoolAddress)
    {
        $this->schoolAddress = (string)$schoolAddress;
    }

    /**
     * @return string
     */
    public function getSchoolEmail()
    {
        return $this->schoolEmail;
    }

    /**
     * @param string $schoolEmail
     */
    public function setSchoolEmail($schoolEmail)
    {
        $this->schoolEmail = (string)$schoolEmail;
    }

    /**
     * @return string
     */
    public function getSchoolPhone()
    {
        return $this->schoolPhone;
    }

    /**
     * @param string $schoolPhone
     */
    public function setSchoolPhone($schoolPhone)
    {
        $this->schoolPhone = (string)$schoolPhone;
    }

    /**
     * @return string
     */
    public function getTimeArrive()
    {
        return $this->timeArrive;
    }

    /**
     * @param string $timeArrive
     */
    public function setTimeArrive($timeArrive)
    {
        $this->timeArrive = (string)$timeArrive;
    }

    /**
     * @return string
     */
    public function getTimeDepart()
    {
        return $this->timeDepart;
    }

    /**
     * @param string $timeDepart
     */
    public function setTimeDepart($timeDepart)
    {
        $this->timeDepart = (string)$timeDepart;
    }

    public function getVolunteers()
    {
        return $this->volunteers;
    }

    /**
     * @param ModuleVolunteer[] $volunteers
     */
    public function setVolunteers($volunteers)
    {
        $this->volunteers = $volunteers;
    }
}