<?php
namespace Airhead\Library\Model;

class ModuleCalendar
{
    /**
     * @var string
     */
    private $date;

    /**
     * @var int
     */
    private $id;

    /**
     * @var int[]
     */
    private $volunteers;

    /**
     * @return string
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param string $date
     */
    public function setDate($date)
    {
        $this->date = (string)$date;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = (int)$id;
    }

    /**
     * @return \int[]
     */
    public function getVolunteers()
    {
        return $this->volunteers;
    }

    /**
     * @param \int[] $volunteers
     */
    public function setVolunteers($volunteers)
    {
        $this->volunteers = $volunteers;
    }
}