<?php
namespace Airhead\Library\Model;

class Content
{
    /**
     * @var ContentData[]
     */
    private $contentData;

    /**
     * @var ContentInf
     */
    private $contentInformation;

    /**
     * @var ContentType
     */
    private $contentType;

    /**
     * @var string
     */
    private $dateCreate;

    /**
     * @var string
     */
    private $dateUpdate;

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $metaUrl;

    /**
     * @var int
     */
    private $position;

    /**
     * @var int
     */
    private $status;

    /**
     * @var User
     */
    private $user;

    /**
     * @return ContentData[]
     */
    public function getContentData()
    {
        return $this->contentData;
    }

    /**
     * @param ContentData[] $contentData
     */
    public function setContentData($contentData)
    {
        $this->contentData = $contentData;
    }

    /**
     * @return ContentInf
     */
    public function getContentInformation()
    {
        return $this->contentInformation;
    }

    /**
     * @param ContentInf $contentInformation
     */
    public function setContentInformation($contentInformation)
    {
        $this->contentInformation = $contentInformation;
    }

    /**
     * @return ContentType
     */
    public function getContentType()
    {
        return $this->contentType;
    }

    /**
     * @param ContentType $contentType
     */
    public function setContentType(ContentType $contentType)
    {
        $this->contentType = $contentType;
    }

    /**
     * @return string
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * @param string $dateCreate
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = (string)$dateCreate;
    }

    /**
     * @return string
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * @param string $dateUpdate
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = (string)$dateUpdate;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = (int)$id;
    }

    /**
     * @return string
     */
    public function getMetaUrl()
    {
        return $this->metaUrl;
    }

    /**
     * @param string $metaUrl
     */
    public function setMetaUrl($metaUrl)
    {
        $this->metaUrl = (string)$metaUrl;
    }

    /**
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param int $position
     */
    public function setPosition($position)
    {
        $this->position = (int)$position;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = (int)$status;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }
}