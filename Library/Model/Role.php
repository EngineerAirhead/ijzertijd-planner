<?php
namespace Airhead\Library\Model;

class Role
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var object
     */
    private $rights;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = (int)$id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = (string)$name;
    }

    /**
     * @return object
     */
    public function getRights()
    {
        return $this->rights;
    }

    /**
     * @param object $rights
     */
    public function setRights($rights)
    {
        $this->rights = (object)$rights;
    }
}