<?php
namespace Airhead\Library\Repository;

use Airhead\Library\Model\Role;

class RoleRepository extends BaseRepository
{
    /**
     * @return Role[]
     */
    public function findAll()
    {
        $roles = $this->db->get('user_role');
        if (count($roles) > 0) {
            foreach ($roles as $key => $role) {
                $roles[$key] = $this->buildRole($role);
            }
        }

        return $roles;
    }

    /**
     * @param $roleId
     * @return Role|bool
     */
    public function findById($roleId)
    {
        $this->db->where('id', $roleId);
        $result = $this->db->getOne('user_role', '*');

        if (is_null($result) === true) {
            return false;
        }

        return $this->buildRole($result);
    }

    /**
     * @param mixed[] $roleData
     * @return Role
     */
    private function buildRole($roleData)
    {
        $role = new Role();
        $role->setId($roleData['id']);
        $role->setName($roleData['name']);
        $role->setRights(json_decode(unserialize($roleData['rights'])));

        return $role;
    }
}