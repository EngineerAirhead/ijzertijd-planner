<?php
namespace Airhead\Library\Repository;

class DonationRepository extends BaseRepository
{
    /**
     * @return int
     */
    public function getTotalDonationAmount()
    {
        $donationResult = $this->db->rawQuery('SELECT SUM(`amount`) as `amount` FROM `donation` LIMIT 1');
        if (count($donationResult) == 1) {
            return $donationResult[0]['amount'];
        }

        return 0;
    }
}