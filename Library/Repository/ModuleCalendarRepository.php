<?php
namespace Airhead\Library\Repository;

use Airhead\Library\Model\ModuleCalendar;

class ModuleCalendarRepository extends BaseRepository
{
    /**
     * @param string[] $inputData
     * @return int|bool
     */
    public function create($inputData)
    {
        $this->db->startTransaction();

        $moduleVolunteerId = $this->db->insert('module_calendar', [
            'date' => date('Y-m-d', strtotime($inputData['date'])),
            'volunteers' => serialize(json_encode($inputData['volunteers']))
        ]);

        if($moduleVolunteerId === false) {
            $this->db->rollback();
            return false;
        }

        $this->db->commit();

        return $moduleVolunteerId;
    }

    /**
     * @param string[] $inputData
     * @param int $id
     * @return bool
     */
    public function update($inputData, $id)
    {
        $this->db->startTransaction();

        $this->db->where('id', $id);
        $update = $this->db->update('module_calendar', [
            'volunteers' => serialize(json_encode($inputData['volunteers']))
        ], 1);

        if($update === false) {
            $this->db->rollback();
            return false;
        }

        return $this->db->commit();
    }

    /**
     * @param int $id
     * @return bool
     */
    public function delete($id)
    {
        $this->db->startTransaction();

        $this->db->where('id', $id);
        $delete = $this->db->delete('module_calendar', 1);
        if ($delete === false) {
            $this->db->rollback();
            return false;
        }

        return $this->db->commit();
    }

    /**
     * @param string $date
     * @return bool
     */
    public function deleteByDate($date)
    {
        $this->db->startTransaction();

        $this->db->where('date', date('Y-m-d', strtotime($date)));
        $delete = $this->db->delete('module_calendar', 1);
        if ($delete === false) {
            $this->db->rollback();
            return false;
        }

        return $this->db->commit();
    }

    /**
     * @param string $startDate
     * @param string $endDate
     * @return array|bool
     */
    public function findBetweenDates($startDate, $endDate)
    {
        $this->db->where('date', [date('Y-m-d', strtotime($startDate)), date('Y-m-d', strtotime($endDate))], 'BETWEEN');
        $scheduled = $this->db->get('module_calendar');

        if (is_null($scheduled) === false) {
            $return = [];
            foreach ($scheduled as $schedule) {
                $return[] = $schedule['date'];
            }

            return $return;
        }

        return false;
    }

    /**
     * @return ModuleCalendar[]|bool
     */
    public function findAll()
    {
        $this->db->orderBy('name', 'ASC');
        $volunteers = $this->db->get('module_calendar');

        if (is_null($volunteers) === false) {
            $return = [];
            foreach ($volunteers as $volunteer) {
                $return[] = $this->buildModuleCalendar($volunteer);
            }

            return $return;
        }

        return false;
    }

    /**
     * @param int $id
     * @return ModuleCalendar|bool
     */
    public function findById($id)
    {
        $this->db->where('id', (int)$id);
        $moduleVolunteer = $this->db->getOne('module_calendar');

        if ($moduleVolunteer === null) {
            return false;
        }

        return $this->buildModuleCalendar($moduleVolunteer);
    }

    /**
     * @param $date
     * @return ModuleCalendar|null
     */
    public function findByDate($date)
    {
        $this->db->where('date', (string)$date);
        $moduleVolunteer = $this->db->getOne('module_calendar');

        if ($moduleVolunteer === null) {
            return null;
        }

        return $this->buildModuleCalendar($moduleVolunteer);
    }

    /**
     * @param array $data
     * @return ModuleCalendar
     */
    private function buildModuleCalendar($data)
    {
        $moduleCalendar = new ModuleCalendar();

        $moduleCalendar->setId($data['id']);
        $moduleCalendar->setDate($data['date']);
        $moduleCalendar->setVolunteers(json_decode(unserialize($data['volunteers']), true));

        return $moduleCalendar;
    }
}