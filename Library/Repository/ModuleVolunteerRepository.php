<?php
namespace Airhead\Library\Repository;

use Airhead\Library\Model\ModuleVolunteer;

class ModuleVolunteerRepository extends BaseRepository
{
    /**
     * @param string[] $inputData
     * @return int|bool
     */
    public function create($inputData)
    {
        $this->db->startTransaction();

        $moduleVolunteerId = $this->db->insert('module_volunteer', [
            'name' => $inputData['name'],
            'email' => $inputData['email'],
            'preferred_days' => serialize(json_encode($inputData['preferred_days']))
        ]);

        if($moduleVolunteerId === false) {
            $this->db->rollback();
            return false;
        }

        $this->db->commit();

        return $moduleVolunteerId;
    }

    /**
     * @param string[] $inputData
     * @param int $id
     * @return bool
     */
    public function update($inputData, $id)
    {
        $this->db->startTransaction();

        $this->db->where('id', $id);
        $update = $this->db->update('module_volunteer', [
            'name' => $inputData['name'],
            'email' => $inputData['email'],
            'preferred_days' => serialize(json_encode($inputData['preferred_days']))
        ], 1);

        if($update === false) {
            $this->db->rollback();
            return false;
        }

        return $this->db->commit();
    }

    /**
     * @param int $id
     * @return bool
     */
    public function delete($id)
    {
        $this->db->startTransaction();

        $this->db->where('id', $id);
        $delete = $this->db->delete('module_volunteer', 1);
        if ($delete === false) {
            $this->db->rollback();
            return false;
        }

        return $this->db->commit();
    }

    /**
     * @return ModuleVolunteer[]|bool
     */
    public function findAll()
    {
        $this->db->orderBy('name', 'ASC');
        $volunteers = $this->db->get('module_volunteer');

        if (is_null($volunteers) === false) {
            $return = [];
            foreach ($volunteers as $volunteer) {
                $return[] = $this->buildModuleVolunteer($volunteer);
            }

            return $return;
        }

        return false;
    }

    /**
     * @param int $id
     * @return ModuleVolunteer|bool
     */
    public function findById($id)
    {
        $this->db->where('id', (int)$id);
        $moduleVolunteer = $this->db->getOne('module_volunteer');

        if ($moduleVolunteer === null) {
            return false;
        }

        return $this->buildModuleVolunteer($moduleVolunteer);
    }

    /**
     * @param array $data
     * @return ModuleVolunteer
     */
    private function buildModuleVolunteer($data)
    {
        $moduleVolunteer = new ModuleVolunteer();

        $moduleVolunteer->setEmail($data['email']);
        $moduleVolunteer->setId($data['id']);
        $moduleVolunteer->setName($data['name']);
        $moduleVolunteer->setPreferredDays(json_decode(unserialize($data['preferred_days'])));

        return $moduleVolunteer;
    }
}