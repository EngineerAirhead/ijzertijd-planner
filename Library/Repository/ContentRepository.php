<?php
namespace Airhead\Library\Repository;

use Airhead\Library\Framework\Sanitize;
use Airhead\Library\Model\Content;
use Airhead\Library\Model\ContentData;
use Airhead\Library\Model\ContentInf;
use Airhead\Library\Model\User;

class ContentRepository extends BaseRepository
{
    /**
     * @param string[] $inputData
     * @param string $type
     * @param int $userId
     * @return int|bool
     */
    public function create($inputData, $type, $userId)
    {
        // Fetch type ID
        $this->db->where('code', $type);
        $contentType = $this->db->getOne('content_type');
        if (is_null($contentType) === true) {
            return false;
        }

        $this->db->startTransaction();

        $contentId = $this->db->insert('content', [
            'id_user' => $userId,
            'id_type' => $contentType['id'],
            'meta_url' => Sanitize::sanitizeURLString($inputData['title']), // TODO: Check for duplicates
            'position' => 0,
            'date_create' => date('Y-m-d H:i:s'),
            'date_update' => date('Y-m-d H:i:s'),
            'status' => (int)$inputData['status'],
        ]);

        if($contentId === false) {
            $this->db->rollback();
            return false;
        }

        $contentInfId = $this->db->insert('content_inf', [
            'id_content' => $contentId,
            'title' => (isset($inputData['title']) === true) ? $inputData['title'] : '',
            'sub_title' => (isset($inputData['sub_title']) === true) ? $inputData['sub_title'] : '',
            'menu_title' => (isset($inputData['menu_title']) === true) ? $inputData['menu_title'] : '',
            'preview' => (isset($inputData['preview']) === true) ? $inputData['preview'] : '',
            'body' => (isset($inputData['body']) === true) ? $inputData['body'] : ''
        ]);
        if ($contentInfId === false) {
            $this->db->rollback();
            return false;
        }

        foreach ($inputData as $key => $value) {
            if (substr($key, 0, 6) === 'extra_') {
                $contentDataId = $this->db->insert('content_data', [
                    'id_content' => $contentId,
                    'data' => $value,
                    'field' => substr($key, 6)
                ]);
                if ($contentDataId === false) {
                    $this->db->rollback();
                    return false;
                }
            }
        }

        $this->db->commit();

        return $contentId;
    }

    /**
     * @param Content $content
     * @param string[] $inputData
     * @return bool
     */
    public function update(Content $content, $inputData)
    {
        $this->db->startTransaction();

        $contentData = [
            'date_update' => date('Y-m-d H:i:s'),
            'status' => $inputData['status']
        ];
        $contentInfData = [
            'title' => (isset($inputData['title']) === true) ? $inputData['title'] : '',
            'sub_title' => (isset($inputData['sub_title']) === true) ? $inputData['sub_title'] : '',
            'menu_title' => (isset($inputData['menu_title']) === true) ? $inputData['menu_title'] : '',
            'preview' => (isset($inputData['preview']) === true) ? $inputData['preview'] : '',
            'body' => (isset($inputData['body']) === true) ? $inputData['body'] : ''
        ];

        // Update content
        $this->db->where('id', $content->getId());
        $update1 = $this->db->update('content', $contentData, 1);
        if ($update1 === false) {
            $this->db->rollback();
            return false;
        }

        // Update content information
        $this->db->where('id', $content->getContentInformation()->getId());
        $update2 = $this->db->update('content_inf', $contentInfData, 1);
        if ($update2 === false) {
            $this->db->rollback();
            return false;
        }

        // Update content data
        foreach ($inputData as $key => $value) {
            if (substr($key, 0, 6) === 'extra_') {
                $this->db->where('id_content', $content->getId());
                $this->db->where('field', substr($key, 6));
                $update3 = $this->db->update('content_data', ['data' => $value], 1);
                if ($update3 === false) {
                    $this->db->rollback();
                    return false;
                }
            }
        }

        $this->db->commit();

        return true;
    }

    /**
     * @param array $sortData
     */
    public function sort($sortData)
    {
        foreach ($sortData as $key => $sortItem) {
            $this->db->where('id', $sortItem['id']);
            $this->db->update('content', ['position' => $key], 1);
        }
    }

    /**
     * @param Content $content
     * @return bool
     */
    public function delete(Content $content)
    {
        $this->db->startTransaction();

        // Remove content information
        $this->db->where('id', $content->getContentInformation()->getId());
        $delete1 = $this->db->delete('content_inf', 1);
        if ($delete1 === false) {
            $this->db->rollback();
            return false;
        }

        // Remove content
        $this->db->where('id', $content->getId());
        $delete2 = $this->db->delete('content', 1);
        if ($delete2 === false) {
            $this->db->rollback();
            return false;
        }

        $this->db->commit();

        return true;
    }

    /**
     * @return array|bool
     */
    public function findAll()
    {
        // Set where statement
        $this->db->join('user', 'user.id = content.id_user', 'INNER');
        $this->db->join('content_type', 'content_type.id = content.id_type', 'INNER');
        $this->db->join('content_inf', 'content_inf.id_content = content.id', 'INNER');
        $this->db->orderBy('content_type.position', 'ASC');
        $this->db->orderBy('content.position', 'ASC');
        $content = $this->db->get('content', null, 'content.*, content_inf.id as contentInfId, user.name');

        // Check for response
        if (is_null($content) === false) {
            $return = [];
            foreach ($content as $page) {
                $return[] = $this->buildContent($page);
            }

            return $return;
        }

        return false;
    }

    /**
     * @param string $type
     * @return Content[]|bool
     */
    public function findContentByType($type)
    {
        $this->db->join('user', 'user.id = content.id_user', 'INNER');
        $this->db->join('content_type', 'content_type.id = content.id_type', 'INNER');
        $this->db->join('content_inf', 'content_inf.id_content = content.id', 'INNER');
        $this->db->where('content_type.code', $type);
        $this->db->orderBy('content.position', 'ASC');
        $content = $this->db->get('content', null, 'content.*, content_inf.id as contentInfId, user.name');

        // Check for response
        if (is_null($content) === false) {
            $return = [];
            foreach ($content as $page) {
                $return[] = $this->buildContent($page);
            }

            return $return;
        }

        return false;
    }

    /**
     * @param int $id
     * @return Content|bool
     */
    public function findById($id)
    {
        $this->db->join('user', 'user.id = content.id_user', 'INNER');
        $this->db->join('content_type', 'content_type.id = content.id_type', 'INNER');
        $this->db->join('content_inf', 'content_inf.id_content = content.id', 'INNER');
        $this->db->where('content.id', $id);
        $this->db->orderBy('content.position', 'ASC');
        $content = $this->db->getOne('content', 'content.*, content_inf.id as contentInfId, user.name');

        // Check for response
        if (is_null($content) === false) {
            return $this->buildContent($content);
        }

        return false;
    }

    /**
     * @param array $value
     * @return Content
     */
    private function buildContent($value)
    {
        $user = new User();
        $user->setName($value['name']);

        $content = new Content();
        $content->setId($value['id']);
        $content->setUser($user);
        $content->setMetaUrl($value['meta_url']);
        $content->setDateCreate($value['date_create']);
        $content->setDateUpdate($value['date_update']);
        $content->setStatus($value['status']);

        $content->setContentType((new ContentTypeRepository($this->db))->findById($value['id_type']));
        $content->setContentInformation($this->getContentInfById($value['contentInfId']));
        $content->setContentData($this->getContentDataById($value['id']));

        return $content;
    }

    /**
     * @param int $contentInfId
     * @return ContentInf|bool
     */
    private function getContentInfById($contentInfId)
    {
        $this->db->where('id', $contentInfId);
        $result = $this->db->getOne('content_inf', '*');

        if (is_null($result) === true) {
            return false;
        }

        $contentInf = new ContentInf();
        $contentInf->setId($contentInfId);
        $contentInf->setTitle($result['title']);
        $contentInf->setSubTitle($result['sub_title']);
        $contentInf->setMenuTitle($result['menu_title']);
        $contentInf->setBody($result['body']);
        $contentInf->setPreview($result['preview']);

        return $contentInf;
    }

    /**
     * @param int $id
     * @return ContentData[]
     */
    private function getContentDataById($id)
    {
        $this->db->where('id_content', $id);
        $content = $this->db->get('content_data');
        $return = [];

        if (is_null($content) === false) {
            foreach ($content as $data) {
                $return[$data['field']] = $this->buildContentData($data);
            }
        }

        return $return;
    }

    /**
     * @param string[] $data
     * @return ContentData
     */
    private function buildContentData($data)
    {
        $contentData = new ContentData();
        $contentData->setId($data['id']);
        $contentData->setData($data['data']);
        $contentData->setDataBig($data['data_big']);
        $contentData->setDataNumeric($data['data_numeric']);
        $contentData->setField($data['field']);

        return $contentData;
    }
}