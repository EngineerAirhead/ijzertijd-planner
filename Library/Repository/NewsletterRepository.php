<?php
namespace Airhead\Library\Repository;

class NewsletterRepository extends BaseRepository
{
    /**
     * @param string $email
     * @return bool
     */
    public function insertEmail($email)
    {
        return $this->db->insert('newsletter', ['email' => strtolower($email)]);
    }

    /**
     * @param string $email
     * @return bool
     */
    public function isDuplicateEmail($email)
    {
        $this->db->where('LOWER(email)', strtolower($email));
        $content = $this->db->get('newsletter');

        if (is_array($content) === true && count($content) > 0) {
            return true;
        }

        return false;
    }
}