<?php
namespace Airhead\Library\Repository;

use Airhead\Library\Model\ModuleAgenda;

class ModuleAgendaRepository extends BaseRepository
{
    /**
     * @param int $id
     * @return bool
     */
    public function confirm($id)
    {
        $this->db->where('id', $id);
        $update = $this->db->update('module_agenda', ['confirmed' => 1], 1);

        if ($update === false) {
            $this->db->rollback();
            return false;
        }

        $this->db->commit();

        return true;
    }

    /**
     * @param string[] $inputData
     * @return int|bool
     */
    public function create($inputData)
    {
        $this->db->startTransaction();

        $moduleAgendaId = $this->db->insert('module_agenda', [
            'date' => date('Y-m-d', strtotime($inputData['date'])),
            'name' => $inputData['name'],
            'school' => $inputData['school'],
            'school_address' => $inputData['address'],
            'school_email' => $inputData['email_school'],
            'school_phone' => $inputData['phone_school'],
            'email' => $inputData['email'],
            'phone' => $inputData['phone'],
            'num_adults' => $inputData['num_adults'],
            'num_kids' => $inputData['num_kids'],
            'class' => $inputData['class'],
            'consume' => $inputData['consume'],
            'time_arrive' => $inputData['time_arrive'],
            'time_depart' => $inputData['time_depart'],
            'information' => $inputData['information'],
            'confirmed' => 0
        ]);

        if($moduleAgendaId === false) {
            $this->db->rollback();
            return false;
        }

        $this->db->commit();

        return $moduleAgendaId;
    }

    /**
     * @return ModuleAgenda[]|bool
     */
    public function findAll()
    {
        $this->db->orderBy('date', 'DESC');
        $this->db->where('confirmed >= 0');
        $agendaItems = $this->db->get('module_agenda');

        if (is_null($agendaItems) === false) {
            $return = [];
            foreach ($agendaItems as $agendaItem) {
                $return[] = $this->buildModuleAgenda($agendaItem);
            }

            return $return;
        }

        return false;
    }

    /**
     * @param string $date
     * @return ModuleAgenda|bool
     */
    public function findByDate($date)
    {
        $this->db->where('date', (string)$date);
        $this->db->where('confirmed >= 0');
        $moduleAgenda = $this->db->getOne('module_agenda');

        if ($moduleAgenda === null) {
            return false;
        }

        return $this->buildModuleAgenda($moduleAgenda);
    }

    /**
     * @param int $id
     * @return ModuleAgenda|bool
     */
    public function findById($id)
    {
        $this->db->where('id', (int)$id);
        $moduleAgenda = $this->db->getOne('module_agenda');

        if ($moduleAgenda === null) {
            return false;
        }

        return $this->buildModuleAgenda($moduleAgenda);
    }

    /**
     * @param string $startDate
     * @param string $endDate
     * @param bool $confirmed
     * @return array|bool
     */
    public function findReservedBetweenDates($startDate, $endDate, $confirmed = true)
    {
        $this->db->where('date', [date('Y-m-d', strtotime($startDate)), date('Y-m-d', strtotime($endDate))], 'BETWEEN');
        $this->db->where('confirmed', ($confirmed === true) ? 1 : 0);
        $scheduled = $this->db->get('module_agenda');

        if (is_null($scheduled) === false) {
            $return = [];
            foreach ($scheduled as $schedule) {
                $return[] = $schedule['date'];
            }

            return $return;
        }

        return false;
    }

    /**
     * @param array $data
     * @return ModuleAgenda
     */
    private function buildModuleAgenda($data)
    {
        $moduleAgenda = new ModuleAgenda();

        $moduleAgenda->setClass($data['class']);
        $moduleAgenda->setConfirmed($data['confirmed'] === 1 ? true : false);
        $moduleAgenda->setConsume($data['consume'] === 1 ? true : false);
        $moduleAgenda->setDate($data['date']);
        $moduleAgenda->setEmail($data['email']);
        $moduleAgenda->setId($data['id']);
        $moduleAgenda->setInformation($data['information']);
        $moduleAgenda->setName($data['name']);
        $moduleAgenda->setNumAdults($data['num_adults']);
        $moduleAgenda->setNumKids($data['num_kids']);
        $moduleAgenda->setPhone($data['phone']);
        $moduleAgenda->setSchool($data['school']);
        $moduleAgenda->setSchoolAddress($data['school_address']);
        $moduleAgenda->setSchoolEmail($data['school_email']);
        $moduleAgenda->setSchoolPhone($data['school_phone']);
        $moduleAgenda->setTimeArrive($data['time_arrive']);
        $moduleAgenda->setTimeDepart($data['time_depart']);

        return $moduleAgenda;
    }
}