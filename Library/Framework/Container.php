<?php
namespace Airhead\Library\Framework;

use Slim\App;
use Slim\Flash\Messages;
use Slim\Router;

class Container
{
    /**
     * @var App
     */
    private static $app;

    /**
     * @var Config
     */
    private static $config;

    /**
     * @var \MysqliDb
     */
    private static $database;

    /**
     * @var Session
     */
    private static $session;

    /**
     * @param App $app
     * @param Config $config
     */
    public function __construct(App $app, Config $config)
    {
        self::$app = $app;
        self::$config = $config;
        self::$database = new \MysqliDb(self::$config->get('db'));
        self::$session = new Session(self::$config->get('sessionToken'));
    }

    /**
     * @return App
     */
    public static function getApp()
    {
        return self::$app;
    }

    /**
     * @return Config
     */
    public static function getConfig()
    {
        return self::$config;
    }

    /**
     * @source https://github.com/joshcam/PHP-MySQLi-Database-Class
     * @return \MysqliDb
     */
    public static function getDatabase()
    {
        return self::$database;
    }

    /**
     * @return Messages
     */
    public static function getFlash()
    {
        return self::$app->getContainer()->get('flash');
    }

    /**
     * @return Router
     */
    public static function getRouter()
    {
        return self::$app->getContainer()->get('router');
    }

    /**
     * @return Session
     */
    public static function getSession()
    {
        return self::$session;
    }
}
