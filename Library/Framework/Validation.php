<?php
namespace Airhead\Library\Framework;

use Respect\Validation\Exceptions\ValidationException;

/**
 * Class Validation
 * @package Airhead\Library\Framework
 * @source http://respect.github.io/Validation/
 */
class Validation
{
    /**
     * @var array
     */
    private $errors = [];

    /**
     * @var array
     */
    private $rules = [];

    /**
     * @var array
     */
    private $messages = [];

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param array $data
     * @return bool
     */
    public function isValid(array $data)
    {
        $isValid = true;

        foreach($data as $key => $value) {
            if (isset($this->rules[$key]) === false) {
                continue;
            }

            try{
                $this->rules[$key]->check($value);
            } catch (ValidationException $e) {
                $error = $e->getMainMessage();
                if (isset($this->messages[$key]) && isset($this->messages[$key][$e->getId()])) {
                    $error = $this->messages[$key][$e->getId()];
                }
                $this->errors[] = $error;
                $isValid = false;
            }
        }

        return $isValid;
    }

    /**
     * @param string $field
     * @param string $rule
     * @param string $message
     */
    public function setCustomErrorMessage($field, $rule, $message)
    {
        if (!isset($this->messages[(string)$field])) {
            $this->messages[(string)$field] = [];
        }

        $this->messages[(string)$field][(string)$rule] = (string)$message;
    }

    /**
     * @param array $rules
     */
    public function setRules(array $rules)
    {
        $this->rules = $rules;
    }
}
