<?php
namespace Airhead\Library\Framework;

class FileBrowser
{
    /**
     * @param string $dir
     * @param int $amount
     * @return string[]
     */
    public static function randomFiles($dir, $amount = 5)
    {
        $files = glob($dir . '/*.*');
        if ($amount == 'all') {
            shuffle($files);
            $returnFiles = $files;
        } else {
            $randKeys = array_rand($files, $amount);

            $returnFiles = array();

            if (count($randKeys) > 0) {
                foreach ($randKeys as $randKey) {
                    $returnFiles[] = $files[$randKey];
                }
            }
        }

        return $returnFiles;
    }
}