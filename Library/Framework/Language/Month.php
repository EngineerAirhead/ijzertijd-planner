<?php
namespace Airhead\Library\Framework\Language;

class Month
{
    /**
     * @param int $month
     * @return string
     */
    public static function getMonthName($month)
    {
        switch ($month) {
            case 1 : return 'januari'; break;
            case 2 : return 'februari'; break;
            case 3 : return 'maart'; break;
            case 4 : return 'april'; break;
            case 5 : return 'mei'; break;
            case 6 : return 'juni'; break;
            case 7 : return 'juli'; break;
            case 8 : return 'augustus'; break;
            case 9 : return 'september'; break;
            case 10 : return 'oktober'; break;
            case 11 : return 'november'; break;
            case 12 : return 'december'; break;
        }

        return '';
    }
}