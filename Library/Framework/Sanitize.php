<?php
namespace Airhead\Library\Framework;

class Sanitize
{
    /**
     * sanitize string for url usage
     *
     * @param $string
     * @param string $separator
     * @return mixed|string
     */
    public static function sanitizeURLString($string, $separator = '-')
    {
        $string = self::normalChars($string);
        $string = str_replace(' ', $separator, $string);
        $string = strtolower($string);

        return $string;
    }

    /**
     * remove special chars from string
     *
     * @param $string
     * @return mixed|string
     */
    public static function normalChars($string)
    {
        $string = htmlentities($string, ENT_QUOTES, 'UTF-8');
        $string = preg_replace('~&([a-z]{1,2})(acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', $string);
        $string = html_entity_decode($string, ENT_QUOTES, 'UTF-8');
        $string = preg_replace(array('~[^0-9a-z]~i', '~[ -]+~'), ' ', $string);
        $string = trim($string, ' -');

        return $string;
    }
}