<?php
namespace Airhead\Library\Framework;

class FormData
{
    /**
     * @var string
     */
    private $formName;

    /**
     * @param string $formName
     */
    public function __construct($formName)
    {
        $this->formName = (string)$formName;
    }
}