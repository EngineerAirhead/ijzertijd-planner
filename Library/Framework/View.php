<?php
namespace Airhead\Library\Framework;

class View
{
    /**
     * @var string
     */
    private $template;

    /**
     * @param string $template
     */
    public function __construct($template)
    {
        $this->template = (string)Container::getConfig()->get('baseDir').DIRECTORY_SEPARATOR.$template.'.phtml';
    }

    /**
     * @return string
     */
    public function parse()
    {
        ob_start();

        include($this->template);

        $output = ob_get_contents();

        ob_end_clean();

        return $output;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->parse();
    }
}