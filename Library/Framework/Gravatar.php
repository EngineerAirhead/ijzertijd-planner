<?php
namespace Airhead\Library\Framework;

class Gravatar
{
    /**
     * @param string $email
     * @param int $size
     * @return string
     */
    public static function getGravatarUrl($email, $size = 80)
    {
        $gravatarHash = md5(strtolower(trim($email)));

        return 'https://www.gravatar.com/avatar/'.$gravatarHash.'.jpg?s='.$size;
    }
}