<?php
namespace Airhead\Wanda\View;

use Airhead\Library\Framework\View;

class ErrorView extends View implements ErrorViewInterface
{
    public function __construct()
    {
        parent::__construct('Wanda/Template/error');
    }
}