<?php
namespace Airhead\Wanda\View\User;

interface FormViewInterface
{
    /**
     * @return string
     */
    public function getFormEmailValue();

    /**
     * @return string
     */
    public function getFormNameValue();

    /**
     * @return bool
     */
    public function getFormResetPasswordValue();

    /**
     * @return int
     */
    public function getFormRoleValue();

    /**
     * @return string
     */
    public function getFormUsernameValue();

    /**
     * @return string
     */
    public function getOverviewUrl();

    /**
     * @return array<int|string>
     */
    public function getRoles();

    /**
     * @param int $size
     * @return string
     */
    public function getUserAvatar($size = 80);

    /**
     * @return string
     */
    public function getValidation();

    /**
     * @return bool
     */
    public function hasUserData();
}