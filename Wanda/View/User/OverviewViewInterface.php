<?php
namespace Airhead\Wanda\View\User;

interface OverviewViewInterface
{
    /**
     * @return string
     */
    public function getAddUserUrl();

    /**
     * @return string
     */
    public function getUserRows();
}