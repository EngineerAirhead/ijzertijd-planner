<?php
namespace Airhead\Wanda\View\User;

use Airhead\Library\Framework\Container;
use Airhead\Library\Framework\Gravatar;
use Airhead\Library\Framework\View;
use Airhead\Library\Model\Role;
use Airhead\Library\Model\User;
use Airhead\Wanda\View\DashboardContentViewInterface;

class FormView extends View implements DashboardContentViewInterface, FormViewInterface
{
    /**
     * @var null|\string[]
     */
    private $inputData;

    /**
     * @var \Airhead\Library\Model\Role[]
     */
    private $roles;

    /**
     * @var User
     */
    private $user;

    /**
     * @var View|null
     */
    private $validationView;

    /**
     * @param Role[] $roles
     * @param User|null $user
     * @param string[]|null $inputData
     * @param View|null $validationView
     */
    public function __construct($roles, User $user = null, $inputData = null, View $validationView = null)
    {
        parent::__construct('Wanda/Template/user/form');

        $this->roles = $roles;
        $this->user = $user;
        $this->inputData = $inputData;
        $this->validationView = $validationView;
    }

    /**
     * @return string
     */
    public function getContentSubTitle()
    {
        if ($this->hasUserData() === true) {
            return $this->user->getUsername();
        }
        return 'He/She/Apache helicopter who may pass!';
    }

    /**
     * @return string
     */
    public function getContentTitle()
    {
        if ($this->hasUserData() === true) {
            return 'Edit user';
        }
        return 'Add user';
    }

    /**
     * @return string
     */
    public function getFormEmailValue()
    {
        if ($this->inputData !== null) {
            return $this->inputData['email'];
        }

        if ($this->user !== null) {
            return $this->user->getEmail();
        }

        return '';
    }

    /**
     * @return string
     */
    public function getFormNameValue()
    {
        if ($this->inputData !== null) {
            return $this->inputData['name'];
        }

        if ($this->user !== null) {
            return $this->user->getName();
        }

        return '';
    }

    /**
     * @return bool
     */
    public function getFormResetPasswordValue()
    {
        if ($this->inputData !== null) {
            return isset($this->inputData['reset_password']) === true;
        }

        if ($this->user !== null) {
            return $this->user->shouldResetPassword();
        }

        return 0;
    }

    /**
     * @return int
     */
    public function getFormRoleValue()
    {
        if ($this->inputData !== null) {
            return (int)$this->inputData['id_user_role'];
        }

        if ($this->user !== null) {
            return $this->user->getRole()->getId();
        }

        return 0;
    }

    /**
     * @return string
     */
    public function getFormUsernameValue()
    {
        if ($this->inputData !== null) {
            return $this->inputData['username'];
        }

        if ($this->user !== null) {
            return $this->user->getUsername();
        }

        return '';
    }

    /**
     * @return string
     */
    public function getOverviewUrl()
    {
        return Container::getRouter()->pathFor('user');
    }

    /**
     * @return array<int|string>
     */
    public function getRoles()
    {
        $return = [];

        foreach ($this->roles as $role) {
            $return[$role->getId()] = $role->getName();
        }

        return $return;
    }

    /**
     * @param int $size
     * @return string
     */
    public function getUserAvatar($size = 80)
    {
        if ($this->inputData !== null) {
            return Gravatar::getGravatarUrl($this->inputData['email'], $size);
        }

        return Gravatar::getGravatarUrl($this->user->getEmail(), $size);
    }

    /**
     * @return string
     */
    public function getValidation()
    {
        if ($this->validationView === null) {
            return '';
        }

        return $this->validationView->parse();
    }

    /**
     * @return bool
     */
    public function hasUserData()
    {
        return $this->user !== null;
    }
}