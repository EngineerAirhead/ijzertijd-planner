<?php
namespace Airhead\Wanda\View\User;

interface TableRowViewInterface
{
    /**
     * @return string
     */
    public function getUserEmail();

    /**
     * @return int
     */
    public function getUserId();

    /**
     * @return string
     */
    public function getUserName();

    /**
     * @return string
     */
    public function getUserUsername();

    /**
     * @return bool
     */
    public function isAllowedToDelete();

    /**
     * @return bool
     */
    public function isAllowedToEdit();

    /**
     * @return bool
     */
    public function isUserAdmin();
}