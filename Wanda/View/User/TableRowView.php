<?php
namespace Airhead\Wanda\View\User;

use Airhead\Wanda\View\OverviewActionInterface;
use Airhead\Library\Framework\Container;
use Airhead\Library\Framework\View;
use Airhead\Library\Model\User;

class TableRowView extends View implements TableRowViewInterface, OverviewActionInterface
{
    /**
     * @var User
     */
    private $currentAdmin;

    /**
     * @var User
     */
    private $user;

    /**
     * @param User $user
     */
    public function __construct(User $user)
    {
        parent::__construct('Wanda/Template/user/table-row');

        $this->currentAdmin = Container::getSession()->get('admin');
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function getDeleteUrl() {
        return Container::getRouter()->pathFor('user-delete', ['id' => $this->getUserId()]);
    }

    /**
     * @return string
     */
    public function getEditUrl()
    {
        return Container::getRouter()->pathFor('user-edit', ['id' => $this->getUserId()]);
    }

    /**
     * @return string
     */
    public function getUserEmail()
    {
        return $this->user->getEmail();
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->user->getId();
    }

    /**
     * @return string
     */
    public function getUserName()
    {
        return $this->user->getName();
    }

    /**
     * @return string
     */
    public function getUserUsername()
    {
        return $this->user->getUsername();
    }

    /**
     * @return bool
     */
    public function isAllowedToDelete()
    {
        return $this->user->isRoot() !== true && $this->user->getId() !== $this->currentAdmin->getId();
    }

    /**
     * @return bool
     */
    public function isAllowedToEdit()
    {
        return $this->user->isRoot() !== true || $this->currentAdmin->isRoot() === true;
    }

    /**
     * @return bool
     */
    public function isUserAdmin()
    {
        return $this->user->isRoot();
    }
}