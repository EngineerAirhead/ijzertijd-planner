<?php
namespace Airhead\Wanda\View\User;

use Airhead\Library\Framework\Container;
use Airhead\Wanda\View\DashboardContentViewInterface;
use Airhead\Library\Framework\View;
use Airhead\Library\Model\User;

class OverviewView extends View implements OverviewViewInterface, DashboardContentViewInterface
{
    /**
     * @var \Airhead\Library\Model\User[]
     */
    private $users;

    /**
     * @param User[] $users
     */
    public function __construct($users)
    {
        parent::__construct('Wanda/Template/user/overview');

        $this->users = $users;
    }

    /**
     * @return string
     */
    public function getContentSubTitle()
    {
        return 'Who let the dogs out?';
    }

    /**
     * @return string
     */
    public function getContentTitle()
    {
        return 'Users';
    }

    /**
     * @return string
     */
    public function getAddUserUrl()
    {
        return Container::getRouter()->pathFor('user-add');
    }

    /**
     * @return string
     */
    public function getUserRows()
    {
        $return = '';
        foreach ($this->users as $user) {
            $return .= (new TableRowView($user))->parse();
        }
        return $return;
    }
}