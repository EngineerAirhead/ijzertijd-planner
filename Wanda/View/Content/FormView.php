<?php
namespace Airhead\Wanda\View\Content;

use Airhead\Library\Framework\Container;
use Airhead\Library\Framework\View;
use Airhead\Library\Model\Content;
use Airhead\Library\Model\ContentData;
use Airhead\Library\Model\ContentType;
use Airhead\Wanda\View\DashboardContentViewInterface;

class FormView extends View implements DashboardContentViewInterface, FormViewInterface
{
    /**
     * @var Content
     */
    private $content;

    /**
     * @var ContentType
     */
    private $contentType;

    /**
     * @var null|\string[]
     */
    private $inputData;

    /**
     * @var View|null
     */
    private $validationView;

    /**
     * @param ContentType $contentType
     * @param Content|null $user
     * @param string[]|null $inputData
     * @param View|null $validationView
     */
    public function __construct(
        ContentType $contentType,
        Content $user = null,
        $inputData = null,
        View $validationView = null
    ) {
        parent::__construct('Wanda/Template/content/form');

        $this->contentType = $contentType;
        $this->content = $user;
        $this->inputData = $inputData;
        $this->validationView = $validationView;
    }

    /**
     * @return string
     */
    public function getContentSubTitle()
    {
        return $this->contentType->getName();
    }

    /**
     * @return string
     */
    public function getContentTitle()
    {
        if ($this->content !== null) {
            return 'Edit content';
        }
        return 'Add content';
    }

    /**
     * @return array
     */
    public function getExtraFormFields()
    {
        if (isset($this->contentType->getFields()->extra) !== true) {
            return [];
        }

        return $this->contentType->getFields()->extra;
    }

    /**
     * @param string $key
     * @return string
     */
    public function getExtraFormFieldValue($key)
    {
        if ($this->inputData !== null) {
            return $this->inputData['extra_' . $key];
        }

        if ($this->content === null || isset($this->content->getContentData()[$key]) === false) {
            return '';
        }

        /** @var ContentData $contentData **/
        $contentData = $this->content->getContentData()[$key];

        return $contentData->getData();
    }

    /**
     * @return string
     */
    public function getFormBodyValue()
    {
        if ($this->inputData !== null) {
            return $this->inputData['body'];
        }

        if ($this->content !== null) {
            return $this->content->getContentInformation()->getBody();
        }

        return '';
    }

    /**
     * @return string
     */
    public function getFormMenuTitleValue()
    {
        if ($this->inputData !== null) {
            return $this->inputData['menu_title'];
        }

        if ($this->content !== null) {
            return $this->content->getContentInformation()->getMenuTitle();
        }

        return '';
    }

    /**
     * @return string
     */
    public function getFormPreviewValue()
    {
        if ($this->inputData !== null) {
            return $this->inputData['preview'];
        }

        if ($this->content !== null) {
            return $this->content->getContentInformation()->getPreview();
        }

        return '';
    }

    /**
     * @return int
     */
    public function getFormStatusValue()
    {
        if ($this->inputData !== null) {
            return (int)$this->inputData['status'];
        }

        if ($this->content !== null) {
            return $this->content->getStatus();
        }

        return 0;
    }

    /**
     * @return string
     */
    public function getFormSubTitleValue()
    {
        if ($this->inputData !== null) {
            return $this->inputData['sub_title'];
        }

        if ($this->content !== null) {
            return $this->content->getContentInformation()->getSubTitle();
        }

        return '';
    }

    /**
     * @return string
     */
    public function getFormTitleValue()
    {
        if ($this->inputData !== null) {
            return $this->inputData['title'];
        }

        if ($this->content !== null) {
            return $this->content->getContentInformation()->getTitle();
        }

        return '';
    }

    /**
     * @return string
     */
    public function getOverviewUrl()
    {
        return Container::getRouter()->pathFor('content', ['type' => $this->contentType->getCode()]);
    }

    /**
     * @return string
     */
    public function getValidation()
    {
        if ($this->validationView === null) {
            return '';
        }

        return $this->validationView->parse();
    }

    /**
     * @param string $key
     * @return bool
     */
    public function isVisibleFormElement($key)
    {
        return array_key_exists($key, $this->contentType->getFields()->default) === true;
    }
}