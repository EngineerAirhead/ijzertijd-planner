<?php
namespace Airhead\Wanda\View\Content;

use Airhead\Wanda\View\OverviewActionInterface;
use Airhead\Library\Framework\Container;
use Airhead\Library\Framework\View;
use Airhead\Library\Model\Content;

class TableRowView extends View implements TableRowViewInterface, OverviewActionInterface
{
    /**
     * @var Content
     */
    private $content;

    /**
     * @param Content $content
     */
    public function __construct(Content $content)
    {
            parent::__construct('Wanda/Template/content/table-row');

        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getDeleteUrl() {
        return Container::getRouter()->pathFor('content-delete', ['id' => $this->getContentId()]);
    }

    /**
     * @return string
     */
    public function getEditUrl()
    {
        return Container::getRouter()->pathFor('content-edit', ['id' => $this->getContentId()]);
    }

    /**
     * @return string
     */
    public function getContentCreateDate()
    {
        return $this->content->getDateCreate();
    }

    /**
     * @return int
     */
    public function getContentId()
    {
        return $this->content->getId();
    }

    /**
     * @return string
     */
    public function getContentOwner()
    {
        return $this->content->getUser()->getName();
    }

    /**
     * @return string
     */
    public function getContentTitle()
    {
        return $this->content->getContentInformation()->getTitle();
    }

    /**
     * @return string
     */
    public function getContentUpdateDate()
    {
        return $this->content->getDateUpdate();
    }

    /**
     * @return bool
     */
    public function isContentPublished()
    {
        return $this->content->getStatus() === 1;
    }
}