<?php
namespace Airhead\Wanda\View\Content;

interface FormViewInterface
{
    /**
     * @return array
     */
    public function getExtraFormFields();

    /***
     * @param string $key
     * @return mixed
     */
    public function getExtraFormFieldValue($key);

    /**
     * @return string
     */
    public function getFormBodyValue();

    /**
     * @return string
     */
    public function getFormMenuTitleValue();

    /**
     * @return string
     */
    public function getFormPreviewValue();

    /**
     * @return string
     */
    public function getFormStatusValue();

    /**
     * @return string
     */
    public function getFormSubTitleValue();

    /**
     * @return string
     */
    public function getFormTitleValue();

    /**
     * @return string
     */
    public function getOverviewUrl();

    /**
     * @return string
     */
    public function getValidation();

    /**
     * @param string $key
     * @return bool
     */
    public function isVisibleFormElement($key);
}