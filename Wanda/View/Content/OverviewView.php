<?php
namespace Airhead\Wanda\View\Content;

use Airhead\Library\Framework\Container;
use Airhead\Wanda\View\DashboardContentViewInterface;
use Airhead\Library\Framework\View;
use Airhead\Library\Model\Content;

class OverviewView extends View implements OverviewViewInterface, DashboardContentViewInterface
{
    /**
     * @var \Airhead\Library\Model\Content[]
     */
    private $content;

    /**
     * @var string
     */
    private $contentType;

    /**
     * @param Content[] $content
     * @param string $contentType
     */
    public function __construct($content, $contentType)
    {
        parent::__construct('Wanda/Template/content/overview');

        $this->content = $content;
        $this->contentType = (string)$contentType;
    }

    /**
     * @return string
     */
    public function getContentSubTitle()
    {
        return $this->contentType;
    }

    /**
     * @return string
     */
    public function getContentTitle()
    {
        return 'Content overview';
    }

    /**
     * @return string
     */
    public function getAddContentUrl()
    {
        return Container::getRouter()->pathFor('content-add', ['type' => $this->contentType]);
    }

    /**
     * @return string
     */
    public function getContentRows()
    {
        $return = '';
        foreach ($this->content as $content) {
            $return .= (new TableRowView($content))->parse();
        }
        return $return;
    }

    /**
     * @return string
     */
    public function getSortContentUrl()
    {
        return Container::getRouter()->pathFor('content-sort');
    }
}