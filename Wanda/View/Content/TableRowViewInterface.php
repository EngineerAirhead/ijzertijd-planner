<?php
namespace Airhead\Wanda\View\Content;

interface TableRowViewInterface
{
    /**
     * @return string
     */
    public function getContentCreateDate();

    /**
     * @return int
     */
    public function getContentId();

    /**
     * @return string
     */
    public function getContentOwner();

    /**
     * @return string
     */
    public function getContentTitle();

    /**
     * @return string
     */
    public function getContentUpdateDate();

    /**
     * @return bool
     */
    public function isContentPublished();
}