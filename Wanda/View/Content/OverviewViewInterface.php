<?php
namespace Airhead\Wanda\View\Content;

interface OverviewViewInterface
{
    /**
     * @return string
     */
    public function getAddContentUrl();

    /**
     * @return string
     */
    public function getContentRows();

    /**
     * @return string
     */
    public function getSortContentUrl();
}