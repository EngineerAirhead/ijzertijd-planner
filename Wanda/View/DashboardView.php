<?php
namespace Airhead\Wanda\View;

use Airhead\Library\Framework\View;

class DashboardView extends View implements DashboardViewInterface
{
    /**
     * @var View
     */
    private $contentView;

    /**
     * @var string[]|null
     */
    private $notificationFlashMessage;

    /**
     * @var View
     */
    private $topNavView;

    /**
     * @var View
     */
    private $leftNavView;

    /**
     * @param View $topNavView
     * @param View $leftNavView
     * @param DashboardContentViewInterface $contentView
     * @param string[]|null $notificationFlashMessage
     */
    public function __construct(
        View $topNavView,
        View $leftNavView,
        DashboardContentViewInterface $contentView,
        $notificationFlashMessage = null
    ) {
        parent::__construct('Wanda/Template/index');

        $this->topNavView = $topNavView;
        $this->leftNavView = $leftNavView;
        $this->contentView = $contentView;
        $this->notificationFlashMessage = $notificationFlashMessage;
    }

    /**
     * @return string
     */
    public function getContentView()
    {
        return $this->contentView->parse();
    }

    /**
     * @return string
     */
    public function getContentViewSubTitle()
    {
        return $this->contentView->getContentSubTitle();
    }

    /**
     * @return string
     */
    public function getContentViewTitle()
    {
        return $this->contentView->getContentTitle();
    }

    /**
     * @return string
     */
    public function getLeftNavView()
    {
        return $this->leftNavView->parse();
    }

    /**
     * @return string
     */
    public function getNotificationFlashMessage()
    {
        if ($this->notificationFlashMessage === null) {
            return '';
        }

        return implode('', $this->notificationFlashMessage);
    }

    /**
     * @return string
     */
    public function getTopNavView()
    {
        return $this->topNavView->parse();
    }
}