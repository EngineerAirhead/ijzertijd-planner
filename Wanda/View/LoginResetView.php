<?php
namespace Airhead\Wanda\View;

use Airhead\Library\Framework\View;

class LoginResetView extends View implements LoginResetViewInterface
{
    /**
     * @var null|\string[]
     */
    private $inputData;

    /**
     * @var null
     */
    private $notificationFlashMessage;

    /**
     * @var null
     */
    private $validationView;

    /**
     * @param string[]|null $inputData
     * @param View|null $validationView
     * @param string[]|null $notificationFlashMessage
     */
    public function __construct($inputData = null, View $validationView = null, $notificationFlashMessage = null)
    {
        parent::__construct('Wanda/Template/login-reset');

        $this->validationView = $validationView;
        $this->notificationFlashMessage = $notificationFlashMessage;
        $this->inputData = $inputData;
    }

    /**
     * @param string $key
     * @return string
     */
    public function getInputValue($key)
    {
        return $this->inputData[(string)$key];
    }

    /**
     * @return string
     */
    public function getNotificationFlashMessage()
    {
        if ($this->notificationFlashMessage === null) {
            return '';
        }

        return implode('', $this->notificationFlashMessage);
    }

    /**
     * @return string
     */
    public function getValidation()
    {
        if ($this->validationView === null) {
            return '';
        }

        return $this->validationView->parse();
    }
}