<?php
namespace Airhead\Wanda\View\Email;

use Airhead\Library\Framework\Container;
use Airhead\Library\Framework\View;

class NewAccount extends View implements NewAccountInterface
{
    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $username;

    /**
     * @param string $username
     * @param string $email
     * @param string $password
     */
    public function __construct($username, $email, $password)
    {
        parent::__construct('Wanda/Template/email/new-account');

        $this->username = (string)$username;
        $this->email = (string)$email;
        $this->password = (string)$password;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getLoginUrl()
    {
        return Container::getConfig()->get('baseUri').Container::getRouter()->pathFor('login');
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }
}