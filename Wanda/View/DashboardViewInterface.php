<?php
namespace Airhead\Wanda\View;

interface DashboardViewInterface
{
    /**
     * @return string
     */
    public function getContentView();

    /**
     * @return string
     */
    public function getContentViewSubTitle();

    /**
     * @return string
     */
    public function getContentViewTitle();

    /**
     * @return string
     */
    public function getLeftNavView();

    /**
     * @return string
     */
    public function getNotificationFlashMessage();

    /**
     * @return string
     */
    public function getTopNavView();
}