<?php
namespace Airhead\Wanda\View;

use Airhead\Library\Framework\View;

class DashboardContentView extends View implements DashboardContentViewInterface
{
    public function __construct()
    {
        parent::__construct('Wanda/Template/dashboard');
    }

    /**
     * @return string
     */
    public function getContentSubTitle()
    {
        return 'Statistics and more';
    }

    /**
     * @return string
     */
    public function getContentTitle()
    {
        return 'Dashboard';
    }
}