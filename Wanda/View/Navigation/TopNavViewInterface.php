<?php
namespace Airhead\Wanda\View\Navigation;

interface TopNavViewInterface
{
    /**
     * @return string
     */
    public function getEmailUrl();

    /**
     * @return string
     */
    public function getLogoutUrl();

    /**
     * @return string
     */
    public function getProfileUrl();

    /**
     * @param int $size
     * @return string
     */
    public function getUserAvatar($size = 80);

    /**
     * @return string
     */
    public function getUserName();
}