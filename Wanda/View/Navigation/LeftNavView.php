<?php
namespace Airhead\Wanda\View\Navigation;

use Airhead\Library\Framework\Container;
use Airhead\Library\Framework\View;
use Airhead\Library\Model\ContentType;
use Airhead\Library\Model\User;

class LeftNavView extends View implements LeftNavViewInterface
{
    /**
     * @var ContentType[]
     */
    private $contentTypes;

    /**
     * @var string
     */
    private $dashboardUrl;

    /**
     * @var string
     */
    private $userOverviewUrl;

    /**
     * @param string $dashboardUrl
     * @param string $userOverviewUrl
     * @param ContentType[] $contentTypes
     */
    public function __construct($dashboardUrl, $userOverviewUrl, $contentTypes)
    {
        parent::__construct('Wanda/Template/navigation/leftnav');

        $this->dashboardUrl = (string)$dashboardUrl;
        $this->userOverviewUrl = (string)$userOverviewUrl;
        $this->contentTypes = $contentTypes;
    }

    /**
     * @param string $key
     * @return string
     */
    public function getContentOverviewUrl($key)
    {
        return Container::getRouter()->pathFor('content', ['type' => $key]);
    }

    /**
     * @return string
     */
    public function getContentTypeOverviewUrl()
    {
        return Container::getRouter()->pathFor('content-type');
    }

    /**
     * @return string
     */
    public function getDashboardUrl()
    {
        return $this->dashboardUrl;
    }

    /**
     * @return string
     */
    public function getLogoutUrl()
    {
        return Container::getRouter()->pathFor('logout');
    }

    /**
     * @param string $module
     * @return string
     */
    public function getModuleUrl($module)
    {
        return Container::getRouter()->pathFor('module', ['module' => $module]);
    }

    /**
     * @return string
     */
    public function getUserOverviewUrl()
    {
        return $this->userOverviewUrl;
    }

    /**
     * @return bool
     */
    public function hasContentTypes()
    {
        return count($this->contentTypes) > 0;
    }

    /**
     * @return bool
     */
    public function isRootUser()
    {
        /** @var User $adminUser */
        $adminUser = Container::getSession()->get('admin');

        return $adminUser->isRoot();
    }
}