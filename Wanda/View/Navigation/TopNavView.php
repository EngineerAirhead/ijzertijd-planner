<?php
namespace Airhead\Wanda\View\Navigation;

use Airhead\Library\Framework\Container;
use Airhead\Library\Framework\Gravatar;
use Airhead\Library\Framework\View;
use Airhead\Library\Model\User;

class TopNavView extends View implements TopNavViewInterface
{
    /**
     * @var User
     */
    private $user;

    /**
     * @param User $user
     */
    public function __construct(User $user)
    {
        parent::__construct('Wanda/Template/navigation/topnav');

        $this->user = $user;
    }

    /**
     * @return string
     */
    public function getEmailUrl()
    {
        return 'http://www.ijzertijdboerderij.nl/webmail';
    }

    /**
     * @return string
     */
    public function getLogoutUrl()
    {
        return Container::getRouter()->pathFor('logout');
    }

    /**
     * @return string
     */
    public function getProfileUrl()
    {
        return Container::getRouter()->pathFor('profile');
    }

    /**
     * @param int $size
     * @return string
     */
    public function getUserAvatar($size = 80)
    {
        return Gravatar::getGravatarUrl($this->user->getEmail(), $size);
    }

    /**
     * @return string
     */
    public function getUserName()
    {
        return $this->user->getName();
    }
}