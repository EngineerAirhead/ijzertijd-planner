<?php
namespace Airhead\Wanda\View\Module\Agenda;

use Airhead\Library\Framework\View;
use Airhead\Library\Model\ModuleAgenda;
use Airhead\Wanda\View\DashboardContentViewInterface;

class OverviewView extends View implements DashboardContentViewInterface, OverviewViewInterface
{
    /**
     * @var ModuleAgenda[]
     */
    private $agendaItems;

    /**
     * @param ModuleAgenda[] $agendaItems
     */
    public function __construct($agendaItems)
    {
        parent::__construct('Wanda/Template/module/agenda/overview');

        $this->agendaItems = $agendaItems;
    }

    /**
     * @return string
     */
    public function getContentSubTitle()
    {
        return 'Zij die leren willen';
    }

    /**
     * @return string
     */
    public function getContentTitle()
    {
        return 'Scholen inschrijvingen';
    }

    public function getAgendaItemRows()
    {
        $return = '';
        foreach ($this->agendaItems as $agendaItem) {
            $return .= (new TableRowView($agendaItem))->parse();
        }
        return $return;
    }
}