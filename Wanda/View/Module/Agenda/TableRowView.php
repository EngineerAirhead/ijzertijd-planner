<?php
namespace Airhead\Wanda\View\Module\Agenda;

use Airhead\Library\Model\ModuleAgenda;
use Airhead\Wanda\View\OverviewActionInterface;
use Airhead\Library\Framework\Container;
use Airhead\Library\Framework\View;

class TableRowView extends View implements TableRowViewInterface, OverviewActionInterface
{
    /**
     * @var ModuleAgenda
     */
    private $agendaItem;

    /**
     * @param ModuleAgenda $agendaItem
     */
    public function __construct(ModuleAgenda $agendaItem)
    {
        parent::__construct('Wanda/Template/module/agenda/table-row');

        $this->agendaItem = $agendaItem;
    }

    /**
     * @return string
     */
    public function getDeleteUrl()
    {
        return '';
    }

    /**
     * @return string
     */
    public function getEditUrl()
    {
        return Container::getRouter()->pathFor('module-edit', [
            'module' => 'agenda',
            'id' => $this->agendaItem->getId()
        ]);
    }

    /**
     * @return int
     */
    public function getAmountOfVisitors()
    {
        return $this->agendaItem->getNumAdults() + $this->agendaItem->getNumKids();
    }

    /**
     * @return string
     */
    public function getDatum()
    {
        return date('d-m-Y', strtotime($this->agendaItem->getDate()));
    }

    /**
     * @return string
     */
    public function getSchool()
    {
        return $this->agendaItem->getSchool();
    }

    /**
     * @return bool
     */
    public function isConfirmed()
    {
        return $this->agendaItem->isConfirmed();
    }
}