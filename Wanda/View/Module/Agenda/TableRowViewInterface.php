<?php
namespace Airhead\Wanda\View\Module\Agenda;

interface TableRowViewInterface
{
    /**
     * @return int
     */
    public function getAmountOfVisitors();

    /**
     * @return string
     */
    public function getDatum();

    /**
     * @return string
     */
    public function getSchool();

    /**
     * @return bool
     */
    public function isConfirmed();
}