<?php
namespace Airhead\Wanda\View\Module\Agenda;

interface OverviewViewInterface
{
    /**
     * @return string
     */
    public function getAgendaItemRows();
}