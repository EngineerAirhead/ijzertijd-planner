<?php
namespace Airhead\Wanda\View\Module\Agenda;

use Airhead\Library\Framework\Container;
use Airhead\Library\Framework\View;
use Airhead\Library\Model\ModuleAgenda;
use Airhead\Wanda\View\DashboardContentViewInterface;

class FormView extends View implements DashboardContentViewInterface, FormViewInterface
{
    /**
     * @var array|null
     */
    private $inputData;

    /**
     * @var ModuleAgenda
     */
    private $moduleAgenda;

    /**
     * @var View
     */
    private $validationView;

    /**
     * @param ModuleAgenda $moduleAgenda
     * @param null|array $inputData
     * @param View $validationView
     */
    public function __construct(ModuleAgenda $moduleAgenda, $inputData = null, View $validationView = null)
    {
        parent::__construct('Wanda/Template/module/agenda/form');

        $this->moduleAgenda = $moduleAgenda;
        $this->inputData = $inputData;
        $this->validationView = $validationView;
    }

    /**
     * @return string
     */
    public function getContentSubTitle()
    {
        return $this->getContactSchool();
    }

    /**
     * @return string
     */
    public function getContentTitle()
    {
        return 'School';
    }

    /**
     * @return string
     */
    public function getContactEmail()
    {
        return $this->moduleAgenda->getEmail();
    }

    /**
     * @return string
     */
    public function getContactName()
    {
        return $this->moduleAgenda->getName();
    }

    /**
     * @return string
     */
    public function getContactPhone()
    {
        return $this->moduleAgenda->getPhone();
    }

    /**
     * @return string
     */
    public function getContactSchool()
    {
        return $this->moduleAgenda->getSchool();
    }

    /**
     * @return string
     */
    public function getContactSchoolAddress()
    {
        return $this->moduleAgenda->getSchoolAddress();
    }

    /**
     * @return string
     */
    public function getContactSchoolEmail()
    {
        return $this->moduleAgenda->getSchoolEmail();
    }

    /**
     * @return string
     */
    public function getContactSchoolPhone()
    {
        return $this->moduleAgenda->getSchoolPhone();
    }

    /**
     * @return string
     */
    public function getExtraInformation()
    {
        return $this->moduleAgenda->getInformation();
    }

    /**
     * @return int
     */
    public function getGroupAdults()
    {
        return $this->moduleAgenda->getNumAdults();
    }

    /**
     * @return int
     */
    public function getGroupChildren()
    {
        return $this->moduleAgenda->getNumKids();
    }

    /**
     * @return string
     */
    public function getGroupClass()
    {
        return $this->moduleAgenda->getClass();
    }

    /**
     * @return bool
     */
    public function getGroupConsume()
    {
        return $this->moduleAgenda->isConsume();
    }

    /**
     * @return string
     */
    public function getOverviewUrl()
    {
        return Container::getRouter()->pathFor('module', ['module' => 'agenda']);
    }

    /**
     * @return string
     */
    public function getTimeArrive()
    {
        return $this->moduleAgenda->getTimeArrive();
    }

    /**
     * @return string
     */
    public function getTimeDepart()
    {
        return $this->moduleAgenda->getTimeDepart();
    }

    /**
     * @return string
     */
    public function getValidation()
    {
        if ($this->validationView === null) {
            return '';
        }

        return $this->validationView->parse();
    }

    /**
     * @return string
     */
    public function getVolunteers()
    {
        if (is_array($this->moduleAgenda->getVolunteers()) === false) {
            return '';
        }

        $return = [];
        foreach ($this->moduleAgenda->getVolunteers() as $volunteer) {
            $return[] = $volunteer->getName();
        }

        return implode('<br />', $return);
    }

    /**
     * @return bool
     */
    public function isConfirmed()
    {
        if ($this->inputData !== null) {
            return isset($this->inputData['confirmed']) === true;
        }

        return $this->moduleAgenda->isConfirmed();
    }
}