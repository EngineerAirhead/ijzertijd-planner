<?php
namespace Airhead\Wanda\View\Module\Volunteer;

use Airhead\Library\Model\ModuleVolunteer;
use Airhead\Wanda\View\OverviewActionInterface;
use Airhead\Library\Framework\Container;
use Airhead\Library\Framework\View;

class TableRowView extends View implements TableRowViewInterface, OverviewActionInterface
{
    /**
     * @var ModuleVolunteer
     */
    private $moduleVolunteer;

    /**
     * @param ModuleVolunteer $moduleVolunteer
     */
    public function __construct(ModuleVolunteer $moduleVolunteer)
    {
        parent::__construct('Wanda/Template/module/volunteer/table-row');

        $this->moduleVolunteer = $moduleVolunteer;
    }

    /**
     * @return string
     */
    public function getDeleteUrl()
    {
        return Container::getRouter()->pathFor('module-delete', [
            'module' => 'volunteer',
            'id' => $this->moduleVolunteer->getId()
        ]);
    }

    /**
     * @return string
     */
    public function getEditUrl()
    {
        return Container::getRouter()->pathFor('module-edit', [
            'module' => 'volunteer',
            'id' => $this->moduleVolunteer->getId()
        ]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->moduleVolunteer->getName();
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->moduleVolunteer->getEmail();
    }

    /**
     * @return string
     */
    public function getPreferredDays()
    {
        $return = [];
        foreach ($this->moduleVolunteer->getPreferredDays() as $day => $bool) {
            if ($bool === true) {
                $return[] = $day;
            }
        }

        return implode(', ', $return);
    }
}