<?php
namespace Airhead\Wanda\View\Module\Volunteer;

interface TableRowViewInterface
{
    /**
     * @return string
     */
    public function getName();

    /**
     * @return string
     */
    public function getEmail();

    /**
     * @return string
     */
    public function getPreferredDays();
}