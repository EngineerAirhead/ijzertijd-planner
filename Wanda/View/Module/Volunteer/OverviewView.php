<?php
namespace Airhead\Wanda\View\Module\Volunteer;

use Airhead\Library\Framework\Container;
use Airhead\Library\Framework\View;
use Airhead\Library\Model\ModuleVolunteer;
use Airhead\Wanda\View\DashboardContentViewInterface;

class OverviewView extends View implements DashboardContentViewInterface, OverviewViewInterface
{
    /**
     * @var ModuleVolunteer[]
     */
    private $moduleVolunteers;

    /**
     * @param ModuleVolunteer[] $moduleVolunteers
     */
    public function __construct($moduleVolunteers)
    {
        parent::__construct('Wanda/Template/module/volunteer/overview');

        $this->moduleVolunteers = $moduleVolunteers;
    }

    /**
     * @return string
     */
    public function getContentSubTitle()
    {
        return 'Zij die helpen willen';
    }

    /**
     * @return string
     */
    public function getContentTitle()
    {
        return 'Vrijwilligers';
    }

    /**
     * @return string
     */
    public function getAddVolunteerUrl()
    {
        return Container::getRouter()->pathFor('module-add', ['module' => 'volunteer']);
    }

    /**
     * @return string
     */
    public function getVolunteerRows()
    {
        $return = '';
        foreach ($this->moduleVolunteers as $moduleVolunteer) {
            $return .= (new TableRowView($moduleVolunteer))->parse();
        }
        return $return;
    }
}