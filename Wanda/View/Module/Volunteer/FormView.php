<?php
namespace Airhead\Wanda\View\Module\Volunteer;

use Airhead\Library\Framework\Container;
use Airhead\Library\Framework\View;
use Airhead\Library\Model\ModuleVolunteer;
use Airhead\Wanda\View\DashboardContentViewInterface;

class FormView extends View implements DashboardContentViewInterface, FormViewInterface
{
    /**
     * @var array|null
     */
    private $inputData;

    /**
     * @var ModuleVolunteer
     */
    private $moduleVolunteer;

    /**
     * @var View
     */
    private $validationView;

    /**
     * @param ModuleVolunteer $moduleVolunteer
     * @param null|array $inputData
     * @param View $validationView
     */
    public function __construct(ModuleVolunteer $moduleVolunteer = null, $inputData = null, View $validationView = null)
    {
        parent::__construct('Wanda/Template/module/volunteer/form');

        $this->moduleVolunteer = $moduleVolunteer;
        $this->inputData = $inputData;
        $this->validationView = $validationView;
    }

    /**
     * @return string
     */
    public function getContentSubTitle()
    {
        return 'niet vrijblijvend';
    }

    /**
     * @return string
     */
    public function getContentTitle()
    {
        return 'Vrijwilliger';
    }

    /**
     * @return bool
     */
    public function getAvailableMonday()
    {
        if ($this->inputData !== null) {
            return (bool)$this->inputData['preferred_days']['MA'];
        }

        if ($this->moduleVolunteer !== null) {
            return $this->moduleVolunteer->getPreferredDays()->MA;
        }

        return false;
    }

    /**
     * @return bool
     */
    public function getAvailableTuesday()
    {
        if ($this->inputData !== null) {
            return (bool)$this->inputData['preferred_days']['DI'];
        }

        if ($this->moduleVolunteer !== null) {
            return $this->moduleVolunteer->getPreferredDays()->DI;
        }

        return false;
    }

    /**
     * @return bool
     */
    public function getAvailableWednesday()
    {
        if ($this->inputData !== null) {
            return (bool)$this->inputData['preferred_days']['WO'];
        }

        if ($this->moduleVolunteer !== null) {
            return $this->moduleVolunteer->getPreferredDays()->WO;
        }

        return false;
    }

    /**
     * @return bool
     */
    public function getAvailableThursday()
    {
        if ($this->inputData !== null) {
            return (bool)$this->inputData['preferred_days']['DO'];
        }

        if ($this->moduleVolunteer !== null) {
            return $this->moduleVolunteer->getPreferredDays()->DO;
        }

        return false;
    }

    /**
     * @return bool
     */
    public function getAvailableFriday()
    {
        if ($this->inputData !== null) {
            return (bool)$this->inputData['preferred_days']['VR'];
        }

        if ($this->moduleVolunteer !== null) {
            return $this->moduleVolunteer->getPreferredDays()->VR;
        }

        return false;
    }

    /**
     * @return bool
     */
    public function getAvailableSaturday()
    {
        if ($this->inputData !== null) {
            return (bool)$this->inputData['preferred_days']['ZA'];
        }

        if ($this->moduleVolunteer !== null) {
            return $this->moduleVolunteer->getPreferredDays()->ZA;
        }

        return false;
    }

    /**
     * @return bool
     */
    public function getAvailableSunday()
    {
        if ($this->inputData !== null) {
            return (bool)$this->inputData['preferred_days']['ZO'];
        }

        if ($this->moduleVolunteer !== null) {
            return $this->moduleVolunteer->getPreferredDays()->ZO;
        }

        return false;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        if ($this->inputData !== null) {
            return $this->inputData['email'];
        }

        if ($this->moduleVolunteer !== null) {
            return $this->moduleVolunteer->getEmail();
        }

        return '';
    }

    /**
     * @return string
     */
    public function getName()
    {
        if ($this->inputData !== null) {
            return $this->inputData['name'];
        }

        if ($this->moduleVolunteer !== null) {
            return $this->moduleVolunteer->getName();
        }

        return '';
    }

    /**
     * @return string
     */
    public function getOverviewUrl()
    {
        return Container::getRouter()->pathFor('module', ['module' => 'volunteer']);
    }

    /**
     * @return string
     */
    public function getValidation()
    {
        if ($this->validationView === null) {
            return '';
        }

        return $this->validationView->parse();
    }
}