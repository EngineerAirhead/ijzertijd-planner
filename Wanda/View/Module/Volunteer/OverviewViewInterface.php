<?php
namespace Airhead\Wanda\View\Module\Volunteer;

interface OverviewViewInterface
{
    /**
     * @return string
     */
    public function getAddVolunteerUrl();

    /**
     * @return string
     */
    public function getVolunteerRows();
}