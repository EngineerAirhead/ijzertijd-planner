<?php
namespace Airhead\Wanda\View\Module\Volunteer;

interface FormViewInterface
{
    /**
     * @return bool
     */
    public function getAvailableMonday();

    /**
     * @return bool
     */
    public function getAvailableTuesday();

    /**
     * @return bool
     */
    public function getAvailableWednesday();

    /**
     * @return bool
     */
    public function getAvailableThursday();

    /**
     * @return bool
     */
    public function getAvailableFriday();

    /**
     * @return bool
     */
    public function getAvailableSaturday();

    /**
     * @return bool
     */
    public function getAvailableSunday();

    /**
     * @return string
     */
    public function getEmail();

    /**
     * @return string
     */
    public function getName();

    /**
     * @return string
     */
    public function getOverviewUrl();

    /**
     * @return string
     */
    public function getValidation();
}