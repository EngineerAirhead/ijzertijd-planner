<?php
namespace Airhead\Wanda\View\Module\Calendar;

interface WeekViewInterface
{
    /**
     * @return string
     */
    public function getWeek();
}