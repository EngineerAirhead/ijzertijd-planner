<?php
namespace Airhead\Wanda\View\Module\Calendar;

interface VolunteerCheckViewInterface
{
    /**
     * @return bool
     */
    public function isChecked();

    /**
     * @return int
     */
    public function getVolunteerId();

    /**
     * @return string
     */
    public function getVolunteerName();

    /**
     * @return string
     */
    public function getVolunteerPreferredDays();
}