<?php
namespace Airhead\Wanda\View\Module\Calendar;

use Airhead\Library\Framework\Container;
use Airhead\Library\Framework\View;
use Airhead\Library\Model\ModuleCalendar;
use Airhead\Library\Model\ModuleVolunteer;

class FormDayView extends View implements FormDayViewInterface
{
    /**
     * @var string
     */
    private $date;

    /**
     * @var ModuleCalendar
     */
    private $moduleCalendar;

    /**
     * @var ModuleVolunteer[]
     */
    private $volunteers;

    /**
     * @param string $date
     * @param ModuleVolunteer[] $volunteers
     * @param ModuleCalendar|null $moduleCalendar
     */
    public function __construct($date, $volunteers, ModuleCalendar $moduleCalendar = null)
    {
        parent::__construct('Wanda/Template/module/calendar/form-day');

        $this->date = (string)$date;
        $this->volunteers = $volunteers;
        $this->moduleCalendar = $moduleCalendar;
    }

    /**
     * @return int
     */
    public function getCalendarId()
    {
        return ($this->moduleCalendar !== null) ? $this->moduleCalendar->getId() : -1;
    }

    /**
     * @return string
     */
    public function getDate()
    {
        return $this->date;
    }

    public function getFormAjaxUrl()
    {
        return Container::getRouter()->pathFor('module-ajax', ['module' => 'calendar', 'call' => 'volunteerForm']);
    }

    /**
     * @return string
     */
    public function getVolunteers()
    {
        $return = '';

        $activeVolunteers = ($this->moduleCalendar !== null) ? $this->moduleCalendar->getVolunteers() : [];

        foreach ($this->volunteers as $volunteer) {
            $return .= (new VolunteerCheckView(
                $volunteer,
                in_array($volunteer->getId(), $activeVolunteers) === true)
            )->parse();
        }

        return $return;
    }
}