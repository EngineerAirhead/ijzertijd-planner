<?php
namespace Airhead\Wanda\View\Module\Calendar;

interface DayViewInterface
{
    /**
     * @return string
     */
    public function getContent();

    /**
     * @param string $format
     * @return string
     */
    public function getDate($format);

    /**
     * @return bool
     */
    public function isEmptyDay();

    /**
     * @return bool
     */
    public function isAvailable();

    /**
     * @return bool
     */
    public function isBooked();

    /**
     * @return bool
     */
    public function isReserved();

    /**
     * @return bool
     */
    public function isScheduled();

    /**
     * @return bool
     */
    public function isToday();
}