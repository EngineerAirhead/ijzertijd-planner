<?php
namespace Airhead\Wanda\View\Module\Calendar;

interface LabelViewInterface
{
    /**
     * @return string[]
     */
    public function getLabels();
}