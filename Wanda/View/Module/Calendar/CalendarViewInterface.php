<?php
namespace Airhead\Wanda\View\Module\Calendar;

interface CalendarViewInterface
{
    /**
     * @return string
     */
    public function getCalendar();

    /**
     * @return string
     */
    public function getHeader();
}