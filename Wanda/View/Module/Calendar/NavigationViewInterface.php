<?php
namespace Airhead\Wanda\View\Module\Calendar;

interface NavigationViewInterface
{
    /**
     * @return array
     */
    public function getMonthSelectOptions();
}