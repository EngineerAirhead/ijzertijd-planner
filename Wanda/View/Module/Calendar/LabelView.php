<?php
namespace Airhead\Wanda\View\Module\Calendar;

use Airhead\Library\Framework\View;

class LabelView extends View implements LabelViewInterface
{
    /**
     * @var string[]
     */
    private $labels;

    /**
     * @param string[] $labels
     */
    public function __construct($labels)
    {
        parent::__construct('Wanda/Template/module/calendar/labels');
        
        $this->labels = $labels;
    }

    /**
     * @return string[]
     */
    public function getLabels()
    {
        return $this->labels;
    }
}