<?php
namespace Airhead\Wanda\View\Module\Calendar;

use Airhead\Library\Framework\View;
use Airhead\Library\Model\ModuleVolunteer;

class VolunteerCheckView extends View implements VolunteerCheckViewInterface
{
    /**
     * @var bool
     */
    private $isActiveForDay;

    /**
     * @var ModuleVolunteer
     */
    private $volunteer;

    /**
     * @param ModuleVolunteer $volunteer
     * @param $isActiveForDay
     */
    public function __construct(ModuleVolunteer $volunteer, $isActiveForDay)
    {
        parent::__construct('Wanda/Template/module/calendar/form-volunteer');

        $this->volunteer = $volunteer;
        $this->isActiveForDay = (bool)$isActiveForDay;
    }

    /**
     * @return bool
     */
    public function isChecked()
    {
        return $this->isActiveForDay;
    }

    /**
     * @return int
     */
    public function getVolunteerId()
    {
        return $this->volunteer->getId();
    }

    /**
     * @return string
     */
    public function getVolunteerName()
    {
        return $this->volunteer->getName();
    }

    /**
     * @return string
     */
    public function getVolunteerPreferredDays()
    {
        return $this->volunteer->getPreferredDays(true);
    }
}