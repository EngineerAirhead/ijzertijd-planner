<?php
namespace Airhead\Wanda\View\Module\Calendar;

interface OverviewViewInterface
{
    /**
     * @return string
     */
    public function getCalendar();

    /**
     * @return string
     */
    public function getCalendarAjaxUrl();

    /**
     * @return string
     */
    public function getFormAjaxUrl();
}