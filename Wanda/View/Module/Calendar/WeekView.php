<?php
namespace Airhead\Wanda\View\Module\Calendar;

use Airhead\Library\Framework\View;

class WeekView extends View implements WeekViewInterface
{
    /**
     * @var object
     */
    private $databaseDates;

    /**
     * @var array<int|Day>
     */
    private $weekDays;

    /**
     * @param array $weekDays
     * @param object $databaseDates
     */
    public function __construct($weekDays, $databaseDates)
    {
        parent::__construct('Wanda/Template/module/calendar/calendar-week');

        $this->weekDays = $weekDays;
        $this->databaseDates = (object)$databaseDates;
    }

    /**
     * @return string
     */
    public function getWeek()
    {
        $content = '';

        foreach ($this->weekDays as $day) {
            $content .= (new DayView($day, $this->databaseDates));
        }
        
        return $content;
    }
    
    
}