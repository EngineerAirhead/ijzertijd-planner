<?php
namespace Airhead\Wanda\View\Module\Calendar;

use Airhead\Library\Framework\Container;
use Airhead\Library\Framework\View;
use Airhead\Wanda\Factory\CalendarFactory;
use Airhead\Wanda\View\DashboardContentViewInterface;

class OverviewView extends View implements DashboardContentViewInterface, OverviewViewInterface
{
    /**
     * @var CalendarFactory
     */
    private $calendarFactory;

    /**
     * @param CalendarFactory $calendarFactory
     */
    public function __construct(CalendarFactory $calendarFactory)
    {
        parent::__construct('Wanda/Template/module/calendar/overview');

        $this->calendarFactory = $calendarFactory;
    }

    /**
     * @return string
     */
    public function getContentSubTitle()
    {
        return 'Zij die helpen willen';
    }

    /**
     * @return string
     */
    public function getContentTitle()
    {
        return 'Vrijwilligers';
    }

    /**
     * @return string
     */
    public function getCalendar()
    {
        return $this->calendarFactory->show();
    }

    /**
     * @return string
     */
    public function getCalendarAjaxUrl()
    {
        return Container::getRouter()->pathFor('module-ajax', ['module' => 'calendar', 'call' => 'calendar']);
    }

    /**
     * @return string
     */
    public function getFormAjaxUrl()
    {
        return Container::getRouter()->pathFor('module-ajax', ['module' => 'calendar', 'call' => 'form']);
    }
}