<?php
namespace Airhead\Wanda\View\Module\Calendar;

interface FormDayViewInterface
{
    /**
     * @return int
     */
    public function getCalendarId();

    /**
     * @return string
     */
    public function getDate();

    /**
     * @return string
     */
    public function getFormAjaxUrl();

    /**
     * @return string
     */
    public function getVolunteers();
}