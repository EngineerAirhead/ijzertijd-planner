<?php
namespace Airhead\Wanda\View;

use Airhead\Library\Framework\Container;
use Airhead\Library\Framework\View;

class PageView extends View implements PageViewInterface
{
    /**
     * @var View
     */
    private $content;

    /**
     * @param View $content
     */
    public function __construct(View $content)
    {
        parent::__construct('Wanda/Template/page');

        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getBaseUrl()
    {
        return Container::getConfig()->get('basePath');
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content->parse();
    }
}