<?php
namespace Airhead\Wanda\View\Config\ContentType;

use Airhead\Library\Framework\Container;
use Airhead\Library\Framework\View;
use Airhead\Library\Model\ContentType;
use Airhead\Wanda\View\DashboardContentViewInterface;

class FormView extends View implements DashboardContentViewInterface, FormViewInterface
{
    /**
     * @var ContentType|null
     */
    private $contentType;

    /**
     * @var null|\string[]
     */
    private $inputData;

    /**
     * @var View|null
     */
    private $validationView;

    /**
     * @param ContentType|null $contentType
     * @param string[]|null $inputData
     * @param View|null $validationView
     */
    public function __construct(ContentType $contentType = null, $inputData = null, View $validationView = null)
    {
        parent::__construct('Wanda/Template/config/content-type/form');

        $this->contentType = $contentType;
        $this->inputData = $inputData;
        $this->validationView = $validationView;
    }

    /**
     * @return string
     */
    public function getContentSubTitle()
    {
        if ($this->contentType !== null) {
            return $this->contentType->getName();
        }
        return 'Keeper of content';
    }

    /**
     * @return string
     */
    public function getContentTitle()
    {
        if ($this->contentType !== null) {
            return 'Edit content type';
        }
        return 'Add content type';
    }

    /**
     * @return string
     */
    public function getContentTypeCode()
    {
        if ($this->inputData !== null) {
            return $this->inputData['code'];
        }
        if ($this->contentType !== null) {
            return $this->contentType->getCode();
        }
        return '';
    }

    /**
     * @return string
     */
    public function getContentTypeName()
    {
        if ($this->inputData !== null) {
            return $this->inputData['name'];
        }
        if ($this->contentType !== null) {
            return $this->contentType->getName();
        }
        return '';
    }

    /**
     * @return mixed
     */
    public function getExtraFields()
    {
        if ($this->inputData !== null && isset($this->inputData['extra']) === true) {
            return json_decode(json_encode($this->inputData['extra']));
        }
        if ($this->contentType !== null && isset($this->contentType->getFields()->extra) === true) {
            return $this->contentType->getFields()->extra;
        }

        return json_decode('{}');
    }

    /**
     * @return string
     */
    public function getOverviewUrl()
    {
        return Container::getRouter()->pathFor('content-type');
    }

    /**
     * @return string
     */
    public function getValidation()
    {
        if ($this->validationView === null) {
            return '';
        }

        return $this->validationView->parse();
    }

    /**
     * @param string $key
     * @return bool
     */
    public function isDefaultChecked($key)
    {
        if ($this->inputData !== null) {
            return isset($this->inputData['default'][$key]) === true;
        }
        if ($this->contentType !== null) {
            return isset($this->contentType->getFields()->default->{$key}) === true;
        }

        return false;
    }
}