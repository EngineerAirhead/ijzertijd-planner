<?php
namespace Airhead\Wanda\View;

interface DashboardContentViewInterface
{
    /**
     * @return string
     */
    public function getContentSubTitle();

    /**
     * @return string
     */
    public function getContentTitle();
}