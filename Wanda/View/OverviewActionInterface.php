<?php
namespace Airhead\Wanda\View;

interface OverviewActionInterface
{
    /**
     * @return string
     */
    public function getDeleteUrl();

    /**
     * @return string
     */
    public function getEditUrl();
}