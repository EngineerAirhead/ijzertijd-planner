<?php
namespace Airhead\Wanda\View;

interface PageViewInterface
{
    /**
     * @return string
     */
    public function getBaseUrl();

    /**
     * @return string
     */
    public function getContent();
}