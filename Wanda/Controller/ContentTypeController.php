<?php
namespace Airhead\Wanda\Controller;

use Airhead\Wanda\Factory\DashboardViewFactory;
use Airhead\Library\Framework\Validation;
use Airhead\Library\Framework\View;
use Airhead\Library\Framework\Container;
use Airhead\Library\Model\ContentType;
use Airhead\Library\Repository\ContentTypeRepository;
use Airhead\Wanda\View\Config\ContentType\FormView;
use Airhead\Wanda\View\Config\ContentType\OverviewView;
use Airhead\Wanda\View\NotificationView;
use Airhead\Wanda\View\PageView;
use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Respect\Validation\Validator;

class ContentTypeController extends BaseController
{
    /**
     * @var DashboardViewFactory
     */
    private $dashboardViewFactory;

    /**
     * @var ContentTypeRepository
     */
    private $contentTypeRepository;

    /**
     * @var Validation
     */
    private $validation;

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     */
    public function __construct(Request $request, Response $response, $args)
    {
        parent::__construct($request, $response, $args);

        $this->contentTypeRepository = new ContentTypeRepository(Container::getDatabase());
        $this->dashboardViewFactory = new DashboardViewFactory($this->contentTypeRepository);
        $this->validation = new Validation();
    }

    /**
     * @return string
     */
    protected function index()
    {
        return (new PageView($this->dashboardViewFactory->buildDashboardView(
            new OverviewView($this->contentTypeRepository->findAll())
        )))->parse();
    }

    /**
     * @return string
     */
    protected function add()
    {
        $inputData = null;

        if ($this->request->getMethod() === 'POST') {
            $inputData = $this->request->getParsedBody();

            $this->validation->setRules([
                'name' => Validator::stringType()->notEmpty()->setName('Name'),
                'code' => Validator::stringType()->notEmpty()->setName('Code')
            ]);

            if ($this->validation->isValid(['name' => $inputData['name'], 'code' => $inputData['code']]) !== true) {
                return $this->returnFormView(null, $inputData, new NotificationView(
                    'danger',
                    $this->validation->getErrors()
                ));
            }

            $contentTypeId = $this->contentTypeRepository->create($inputData);
            if ($contentTypeId === false) {
                return $this->returnFormView(null, $inputData, new NotificationView(
                    'danger',
                    ['Unable to create content type']
                ));
            }

            Container::getFlash()->addMessage(
                'notification',
                new NotificationView('success', ['A new content type has been created'])
            );

            return $this->response->withRedirect(Container::getRouter()->pathFor(
                'content-type-edit',
                ['id' => $contentTypeId]
            ));
        }

        return $this->returnFormView(null, $inputData);
    }

    /**
     * @return string
     */
    protected function edit()
    {
        $inputData = null;

        $contentType = $this->contentTypeRepository->findById($this->args['id']);
        if ($contentType === false) {
            Container::getFlash()->addMessage(
                'notification',
                new NotificationView('danger', ['That content type does not even exist yet!'])
            );
            return $this->response->withRedirect(Container::getRouter()->pathFor('content-type'));
        }

        if ($this->request->getMethod() === 'POST') {
            $inputData = $this->request->getParsedBody();

            $this->validation->setRules([
                'name' => Validator::stringType()->notEmpty()->setName('Name'),
                'code' => Validator::stringType()->notEmpty()->setName('Code')
            ]);

            if ($this->validation->isValid(['name' => $inputData['name'], 'code' => $inputData['code']]) !== true) {
                return $this->returnFormView($contentType, $inputData, new NotificationView(
                    'danger',
                    $this->validation->getErrors()
                ));
            }

            $status = $this->contentTypeRepository->update($contentType, $inputData);
            if ($status === false) {
                return $this->returnFormView($contentType, $inputData, new NotificationView(
                    'danger',
                    ['Unable to update content type']
                ));
            }

            return $this->returnFormView($contentType, $inputData, new NotificationView(
                'success',
                ['Updated the content type']
            ));
        }

        return $this->returnFormView($contentType, $inputData);
    }

    /**
     * @return mixed
     */
    protected function delete()
    {
        $contentType = $this->contentTypeRepository->findById($this->args['id']);
        if ($contentType === false) {
            Container::getFlash()->addMessage(
                'notification',
                new NotificationView('danger', ['That content type does not even exist yet!'])
            );
            return $this->response->withRedirect(Container::getRouter()->pathFor('content-type'));
        }

        $status = $this->contentTypeRepository->delete($contentType);
        if ($status === false) {
            Container::getFlash()->addMessage(
                'notification',
                new NotificationView('danger', ['Could not remove the content type.'])
            );
        } else {
            Container::getFlash()->addMessage(
                'notification',
                new NotificationView('success', ['The content type has been removed.'])
            );
        }

        return $this->response->withRedirect(Container::getRouter()->pathFor('content-type'));
    }

    /**
     * @param ContentType|null $contentType
     * @param string[]|null $inputData
     * @param View|null $notificationView
     * @return string
     */
    private function returnFormView(ContentType $contentType = null, $inputData = null, View $notificationView = null)
    {
        return (new PageView($this->dashboardViewFactory->buildDashboardView(
            new FormView($contentType, $inputData, $notificationView)
        )))->parse();
    }
}