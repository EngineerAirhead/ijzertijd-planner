<?php
namespace Airhead\Wanda\Controller;

use Airhead\Library\Repository\ContentTypeRepository;
use Airhead\Wanda\Factory\DashboardViewFactory;
use Airhead\Library\Framework\Container;
use Airhead\Library\Framework\Validation;
use Airhead\Library\Repository\UserRepository;
use Airhead\Wanda\View\PageView;
use Airhead\Wanda\View\Profile\ProfileView;
use Slim\Http\Request;
use Slim\Http\Response;

class ProfileController extends BaseController
{
    /**
     * @var DashboardViewFactory
     */
    private $dashboardViewFactory;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var Validation
     */
    private $validation;

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     */
    public function __construct(Request $request, Response $response, $args)
    {
        parent::__construct($request, $response, $args);

        $this->userRepository = new UserRepository(Container::getDatabase());
        $this->dashboardViewFactory = new DashboardViewFactory(new ContentTypeRepository(Container::getDatabase()));
        $this->validation = new Validation();
    }

    /**
     * @return string
     */
    protected function index()
    {
        return (new PageView($this->dashboardViewFactory->buildDashboardView(
            new ProfileView($this->userRepository->findById($this->adminLoggedIn->getId()))
        )))->parse();
    }
}