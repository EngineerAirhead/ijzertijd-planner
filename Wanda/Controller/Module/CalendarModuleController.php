<?php
namespace Airhead\Wanda\Controller\Module;

use Airhead\Cosmo\Factory\CalendarDataFactory;
use Airhead\Library\Framework\Container;
use Airhead\Library\Repository\ContentTypeRepository;
use Airhead\Library\Repository\ModuleAgendaRepository;
use Airhead\Library\Repository\ModuleCalendarRepository;
use Airhead\Library\Repository\ModuleVolunteerRepository;
use Airhead\Wanda\Controller\BaseController;
use Airhead\Wanda\Factory\CalendarFactory;
use Airhead\Wanda\Factory\DashboardViewFactory;
use Airhead\Wanda\View\Module\Calendar\FormDayView;
use Airhead\Wanda\View\Module\Calendar\OverviewView;
use Airhead\Wanda\View\PageView;
use Slim\Http\Request;
use Slim\Http\Response;

class CalendarModuleController extends BaseController implements ModuleControllerInterface
{
    /**
     * @var DashboardViewFactory
     */
    private $dashboardViewFactory;

    /**
     * @var ModuleAgendaRepository
     */
    private $moduleAgendaRepository;

    /**
     * @var ModuleCalendarRepository
     */
    private $moduleCalendarRepository;

    /**
     * @var ModuleVolunteerRepository
     */
    private $moduleVolunteerRepository;

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     */
    public function __construct(Request $request, Response $response, $args)
    {
        parent::__construct($request, $response, $args);

        $this->dashboardViewFactory = new DashboardViewFactory(new ContentTypeRepository(Container::getDatabase()));
        $this->moduleAgendaRepository = new ModuleAgendaRepository(Container::getDatabase());
        $this->moduleCalendarRepository = new ModuleCalendarRepository(Container::getDatabase());
        $this->moduleVolunteerRepository = new ModuleVolunteerRepository(Container::getDatabase());
    }

    public function index()
    {
        $month = date("m",time());
        $year = date("Y",time());

        $startDate = $year.'-'.$month.'-01';
        $endDate = date('Y-m-t', strtotime($startDate));

        return (new PageView($this->dashboardViewFactory->buildDashboardView(
            new OverviewView(new CalendarFactory($month, $year, $this->getDatabaseResults($startDate, $endDate)))
        )))->parse();
    }

    /**
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function ajaxCalendar()
    {
        $queryParams = $this->request->getQueryParams();

        $month = (isset($queryParams['month']) === true) ? $queryParams['month'] : date("m",time());
        $year = (isset($queryParams['year']) === true) ? $queryParams['year'] : date("Y",time());

        $startDate = $year.'-'.$month.'-01';
        $endDate = date('Y-m-t', strtotime($startDate));

        $calendarFactory = new CalendarFactory($month, $year, $this->getDatabaseResults($startDate, $endDate));

        return $this->renderSuccess($calendarFactory->show(false));
    }

    /**
     * @param string $startDate
     * @param string $endDate
     * @return \stdClass
     */
    private function getDatabaseResults($startDate, $endDate)
    {
        $calendarData = new \stdClass();
        $calendarData->datesBooked = [];
        $calendarData->datesEnabled = [];
        $calendarData->datesReserved = [];

        $datesReservedResultSet = $this->moduleAgendaRepository->findReservedBetweenDates($startDate, $endDate, false);
        if ($datesReservedResultSet !== false) {
            $calendarData->datesReserved = $datesReservedResultSet;
        }

        $datesBookedResultSet = $this->moduleAgendaRepository->findReservedBetweenDates($startDate, $endDate);
        if ($datesBookedResultSet !== false) {
            $calendarData->datesBooked = $datesBookedResultSet;
        }

        $datesEnabledResultSet = $this->moduleCalendarRepository->findBetweenDates($startDate, $endDate);
        if ($datesEnabledResultSet !== false) {
            $calendarData->datesEnabled = $datesEnabledResultSet;
        }

        return $calendarData;
    }

    /**
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function ajaxForm()
    {
        $queryParams = $this->request->getQueryParams();

        $formDayView = new FormDayView(
            $queryParams['date'],
            $this->moduleVolunteerRepository->findAll(),
            $this->moduleCalendarRepository->findByDate(date('Y-m-d', strtotime($queryParams['date'])))
        );

        return $this->renderSuccess($formDayView->parse());
    }

    /**
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function ajaxVolunteerForm()
    {
        $queryParams = $this->request->getQueryParams();

        if (isset($queryParams['volunteers']) === false) {
            $queryParams['action'] = 'remove';
            if ($this->moduleCalendarRepository->deleteByDate($queryParams['date']) === false) {
                return $this->renderError('Kon de dag niet verwijderen.');
            }

            return $this->renderSuccess($queryParams);
        }

        if ($queryParams['id'] > 0) {
            $queryParams['action'] = 'update';
            if ($this->moduleCalendarRepository->update($queryParams, $queryParams['id']) === false) {
                return $this->renderError('Kon de dag niet opslaan.');
            }
        } else {
            $queryParams['action'] = 'insert';
            $queryParams['id'] = $this->moduleCalendarRepository->create($queryParams);
            if ($queryParams['id'] === false) {
                return $this->renderError('Kon de dag niet opslaan.');
            }
        }

        return $this->renderSuccess($queryParams);
    }

    public function add()
    {
        // TODO: Implement add() method.
    }

    public function edit()
    {
        // TODO: Implement edit() method.
    }

    public function delete()
    {
        // TODO: Implement delete() method.
    }
}