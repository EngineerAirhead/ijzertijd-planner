<?php
namespace Airhead\Wanda\Controller\Module;

use Airhead\Cosmo\View\Email\EmailView;
use Airhead\Cosmo\View\Email\ModuleAgendaView;
use Airhead\Library\Framework\Container;
use Airhead\Library\Framework\View;
use Airhead\Library\Model\ModuleAgenda;
use Airhead\Library\Repository\ContentTypeRepository;
use Airhead\Library\Repository\ModuleAgendaRepository;
use Airhead\Library\Repository\ModuleCalendarRepository;
use Airhead\Library\Repository\ModuleVolunteerRepository;
use Airhead\Wanda\Controller\BaseController;
use Airhead\Wanda\Factory\DashboardViewFactory;
use Airhead\Wanda\View\Email\Email;
use Airhead\Wanda\View\Email\SchoolConfirmationView;
use Airhead\Wanda\View\ErrorView;
use Airhead\Wanda\View\Module\Agenda\FormView;
use Airhead\Wanda\View\Module\Agenda\OverviewView;
use Airhead\Wanda\View\NotificationView;
use Airhead\Wanda\View\PageView;
use Slim\Http\Request;
use Slim\Http\Response;

class AgendaModuleController extends BaseController implements ModuleControllerInterface
{
    /**
     * @var DashboardViewFactory
     */
    private $dashboardViewFactory;

    /**
     * @var ModuleAgendaRepository
     */
    private $moduleAgendaRepository;

    /**
     * @var ModuleCalendarRepository
     */
    private $moduleCalendarRepository;

    /**
     * @var ModuleVolunteerRepository
     */
    private $moduleVolunteerRepository;

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     */
    public function __construct(Request $request, Response $response, $args)
    {
        parent::__construct($request, $response, $args);

        $this->dashboardViewFactory = new DashboardViewFactory(new ContentTypeRepository(Container::getDatabase()));
        $this->moduleAgendaRepository = new ModuleAgendaRepository(Container::getDatabase());
        $this->moduleCalendarRepository = new ModuleCalendarRepository(Container::getDatabase());
        $this->moduleVolunteerRepository = new ModuleVolunteerRepository(Container::getDatabase());
    }

    public function index()
    {
        return (new PageView($this->dashboardViewFactory->buildDashboardView(
            new OverviewView($this->moduleAgendaRepository->findAll())
        )))->parse();
    }

    /**
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function add()
    {
        return $this->renderAsPageNotFound(new PageView(new ErrorView()));
    }

    /**
     * @return string
     */
    public function edit()
    {
        $inputData = null;

        $moduleAgenda = $this->moduleAgendaRepository->findById($this->args['id']);
        if ($moduleAgenda === false) {
            Container::getFlash()->addMessage(
                'notification',
                new NotificationView('danger', ['Aanmelding niet gevonden'])
            );
            return $this->response->withRedirect(Container::getRouter()->pathFor('module', ['module' => 'agenda']));
        }

        $calendarInformation = $this->moduleCalendarRepository->findByDate($moduleAgenda->getDate());
        if ($calendarInformation !== false) {
            // Get volunteers
            $volunteers = [];
            foreach ($calendarInformation->getVolunteers() as $volunteerId) {
                $volunteer = $this->moduleVolunteerRepository->findById($volunteerId);
                if ($volunteer !== false) {
                    $volunteers[] = $volunteer;
                }
            }
            $moduleAgenda->setVolunteers($volunteers);
        }

        if ($this->request->getMethod() === 'POST') {
            $inputData = $this->request->getParsedBody();

            if (isset($inputData['confirmed']) !== true || $moduleAgenda->isConfirmed() === true) {
                return $this->returnFormView($moduleAgenda, $inputData);
            }

            $updateStatus = $this->moduleAgendaRepository->confirm($this->args['id']);
            if ($updateStatus === false) {
                return $this->returnFormView($moduleAgenda, $inputData, new NotificationView(
                    'danger',
                    ['Ik kan de aanmelding momenteel niet bevestigen']
                ));
            }

            // Send confirmation email to customer
            $mail = new \PHPMailer();

            $mail->setFrom('planning@ijzertijdboerderij.nl', 'Ijzertijd boerderij Dongen planning');
            $mail->addAddress($moduleAgenda->getEmail(), $moduleAgenda->getName());

            // Add volunteers to confirmation
            $mail->addBCC('planning@ijzertijdboerderij.nl', 'Ijzertijd boerderij Dongen planning');
            foreach ($moduleAgenda->getVolunteers() as $volunteer) {
                $mail->addBCC($volunteer->getEmail(), $volunteer->getName());
            }

            $mail->isHTML(true);

            $mail->Subject = 'Uw school bevestiging bij ijzertijd boerderij Dongen';
            $mail->Body = (new EmailView(
                'Uw school bevestiging bij ijzertijd boerderij Dongen',
                new SchoolConfirmationView($moduleAgenda)
            ))->parse();

            $mail->send();

            return $this->returnFormView($moduleAgenda, $inputData, new NotificationView('success', ['Aanmelding bevestigd']));
        }

        return $this->returnFormView($moduleAgenda, $inputData);
    }

    /**
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function delete()
    {
        return $this->renderAsPageNotFound(new PageView(new ErrorView()));
    }

    /**
     * @param ModuleAgenda $moduleAgenda
     * @param array|null $inputData
     * @param View|null $notificationView
     * @return string
     */
    private function returnFormView(ModuleAgenda $moduleAgenda, $inputData = null, View $notificationView = null)
    {
        return (new PageView($this->dashboardViewFactory->buildDashboardView(
            new FormView($moduleAgenda, $inputData, $notificationView)
        )))->parse();
    }
}