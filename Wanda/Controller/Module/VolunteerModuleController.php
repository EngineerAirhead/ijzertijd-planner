<?php
namespace Airhead\Wanda\Controller\Module;

use Airhead\Library\Framework\Container;
use Airhead\Library\Framework\Validation;
use Airhead\Library\Framework\View;
use Airhead\Library\Model\ModuleVolunteer;
use Airhead\Library\Repository\ContentTypeRepository;
use Airhead\Library\Repository\ModuleVolunteerRepository;
use Airhead\Wanda\Controller\BaseController;
use Airhead\Wanda\Factory\DashboardViewFactory;
use Airhead\Wanda\View\Module\Volunteer\FormView;
use Airhead\Wanda\View\Module\Volunteer\OverviewView;
use Airhead\Wanda\View\NotificationView;
use Airhead\Wanda\View\PageView;
use Respect\Validation\Validator;
use Slim\Http\Request;
use Slim\Http\Response;

class VolunteerModuleController extends BaseController implements ModuleControllerInterface
{
    /**
     * @var DashboardViewFactory
     */
    private $dashboardViewFactory;

    /**
     * @var ModuleVolunteerRepository
     */
    private $moduleVolunteerRepository;

    /**
     * @var Validation
     */
    private $validation;

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     */
    public function __construct(Request $request, Response $response, $args)
    {
        parent::__construct($request, $response, $args);

        $this->dashboardViewFactory = new DashboardViewFactory(new ContentTypeRepository(Container::getDatabase()));
        $this->moduleVolunteerRepository = new ModuleVolunteerRepository(Container::getDatabase());
        $this->validation = new Validation();

        $this->validation->setRules([
            'name' => Validator::stringType()->notEmpty()->setName('Naam'),
            'email' => Validator::email()->notEmpty()->setName('Email'),
            'preferred_days' => Validator::arrayType()->notEmpty()->setName('Dagen beschikbaar')
        ]);

        $this->validation->setCustomErrorMessage('name', 'notEmpty', 'Naam is leeg');
        $this->validation->setCustomErrorMessage('email', 'email', 'Email adres is ongeldig');
    }

    public function index()
    {
        return (new PageView($this->dashboardViewFactory->buildDashboardView(
            new OverviewView($this->moduleVolunteerRepository->findAll())
        )))->parse();
    }

    /**
     * @return string
     */
    public function add()
    {
        $inputData = null;

        if ($this->request->getMethod() === 'POST') {
            $inputData = $this->request->getParsedBody();

            if ($this->validation->isValid($inputData) !== true) {
                return $this->returnFormView(null, $inputData, new NotificationView(
                    'danger',
                    $this->validation->getErrors()
                ));
            }

            foreach ($inputData['preferred_days'] as $key => $bool) {
                $inputData['preferred_days'][$key] = (bool)$bool;
            }

            $moduleVolunteerId = $this->moduleVolunteerRepository->create($inputData);
            if ($moduleVolunteerId === false) {
                return $this->returnFormView(null, $inputData, new NotificationView(
                    'danger',
                    ['Kon de vrijwilliger niet aanmaken']
                ));
            }

            Container::getFlash()->addMessage(
                'notification',
                new NotificationView('success', ['Vrijwilliger is aangemaakt'])
            );

            return $this->response->withRedirect(Container::getRouter()->pathFor(
                'module-edit',
                ['module' => 'volunteer', 'id' => $moduleVolunteerId]
            ));
        }

        return $this->returnFormView(null, $inputData);
    }

    public function edit()
    {
        $inputData = null;

        $moduleVolunteer = $this->moduleVolunteerRepository->findById($this->args['id']);
        if ($moduleVolunteer === false) {
            Container::getFlash()->addMessage(
                'notification',
                new NotificationView('danger', ['Vrijwilliger bestaat (nog) niet'])
            );
            return $this->response->withRedirect(Container::getRouter()->pathFor('module', ['module' => 'volunteer']));
        }

        if ($this->request->getMethod() === 'POST') {
            $inputData = $this->request->getParsedBody();

            if ($this->validation->isValid($inputData) !== true) {
                return $this->returnFormView($moduleVolunteer, $inputData, new NotificationView(
                    'danger',
                    $this->validation->getErrors()
                ));
            }

            foreach ($inputData['preferred_days'] as $key => $bool) {
                $inputData['preferred_days'][$key] = (bool)$bool;
            }

            $status = $this->moduleVolunteerRepository->update($inputData, $this->args['id']);
            if ($status === false) {
                return $this->returnFormView($moduleVolunteer, $inputData, new NotificationView(
                    'danger',
                    ['Ik kon de vrijwilliger niet opslaan']
                ));
            }

            return $this->returnFormView($moduleVolunteer, $inputData, new NotificationView(
                'success',
                ['Vrijwilliger is opgeslagen']
            ));
        }

        return $this->returnFormView($moduleVolunteer, $inputData);
    }

    /**
     * @return mixed
     */
    public function delete()
    {
        if ($this->moduleVolunteerRepository->delete($this->args['id']) === false) {
            Container::getFlash()->addMessage(
                'notification',
                new NotificationView('danger', ['Ik kon de vrijwilliger niet verwijderen.'])
            );
        } else {
            Container::getFlash()->addMessage(
                'notification',
                new NotificationView('success', ['Vrijwilliger is uit het systeem gehaald'])
            );
        }

        return $this->response->withRedirect(Container::getRouter()->pathFor('module', ['module' => 'volunteer']));
    }

    /**
     * @param ModuleVolunteer|null $moduleVolunteer
     * @param string[]|null $inputData
     * @param View|null $notificationView
     * @return string
     */
    private function returnFormView(ModuleVolunteer $moduleVolunteer = null, $inputData = null, View $notificationView = null)
    {
        return (new PageView($this->dashboardViewFactory->buildDashboardView(
            new FormView($moduleVolunteer, $inputData, $notificationView)
        )))->parse();
    }
}