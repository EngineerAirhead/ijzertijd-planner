<?php
namespace Airhead\Wanda\Controller;

use Airhead\Library\Framework\Container;
use Airhead\Library\Framework\View;
use Airhead\Library\Model\User;
use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Route;

class BaseController
{
    /**
     * @var bool|User
     */
    protected $adminLoggedIn;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var Response
     */
    protected $response;

    /**
     * @var array
     */
    protected $args;

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     */
    public function __construct(Request $request, Response $response, $args)
    {
        $this->request = $request;
        $this->response = $response;
        $this->args = $args;

        $this->adminLoggedIn = Container::getSession()->get('admin');
    }

    /**
     * @param $method
     * @param $arguments
     * @return mixed
     */
    public function __call($method,$arguments) {
        /** @var Route $route */
        $route = $this->request->getAttribute('route');
        if ($this->adminLoggedIn === false && $route->getName() !== 'login') {
            Container::getSession()->set(
                'redirectUrl',
                Container::getConfig()->get('basePath') . $this->request->getUri()->getPath()
            );
            return $this->response->withRedirect(
                Container::getConfig()->get('baseUri').Container::getRouter()->pathFor('login')
            );
        }

        if ($this->adminLoggedIn !== false &&
            $this->adminLoggedIn->shouldResetPassword() === true &&
            $route->getName() !== 'login-reset') {
            Container::getSession()->set(
                'redirectUrl',
                Container::getConfig()->get('basePath') . $this->request->getUri()->getPath()
            );
            return $this->response->withRedirect(
                Container::getConfig()->get('baseUri').Container::getRouter()->pathFor('login-reset')
            );
        }

        if(method_exists($this, $method) === false) {
            throw new \BadMethodCallException('Method ' . $method . ' not found in Controller ' . get_class($this));
        }

        return call_user_func_array(array($this,$method),$arguments);
    }

    /**
     * @param string $feedback
     * @return Response
     */
    public function renderError($feedback)
    {
        return $this->response->withJson(['status' => 'error', 'feedback' => $feedback]);
    }

    /**
     * @param View $view
     * @return Response
     */
    public function renderAsPageNotFound(View $view)
    {
        $this->response->getBody()->write($view->parse());
        return $this->response->withStatus(404);
    }

    /**
     * @param mixed $feedback
     * @return Response
     */
    public function renderSuccess($feedback)
    {
        return $this->response->withJson(
            ['status' => 'success', 'feedback' => $feedback],
            null,
            JSON_HEX_QUOT | JSON_HEX_TAG
        );
    }
}