<?php
namespace Airhead\Wanda\Controller;

use Airhead\Library\Repository\ContentTypeRepository;
use Airhead\Wanda\Factory\DashboardViewFactory;
use Airhead\Wanda\View\DashboardContentView;
use Airhead\Wanda\View\PageView;
use Airhead\Library\Framework\Container;
use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class IndexController extends BaseController
{
    /**
     * @var DashboardViewFactory
     */
    private $dashboardViewFactory;

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     */
    public function __construct(Request $request, Response $response, $args)
    {
        parent::__construct($request, $response, $args);

        $this->dashboardViewFactory = new DashboardViewFactory(new ContentTypeRepository(Container::getDatabase()));
    }

    /**
     * @return string
     */
    protected function index()
    {
        return (new PageView($this->dashboardViewFactory->buildDashboardView(new DashboardContentView())))->parse();
    }
}