<?php
namespace Airhead\Wanda\Controller;

use Airhead\Library\Framework\Validation;
use Airhead\Library\Framework\View;
use Airhead\Library\Model\Content;
use Airhead\Library\Repository\ContentTypeRepository;
use Airhead\Library\Framework\Container;
use Airhead\Library\Repository\ContentRepository;
use Airhead\Wanda\Factory\DashboardViewFactory;
use Airhead\Wanda\View\Content\FormView;
use Airhead\Wanda\View\NotificationView;
use Airhead\Wanda\View\PageView;
use Airhead\Wanda\View\Content\OverviewView;
use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Respect\Validation\Validator;

class ContentController extends BaseController
{
    /**
     * @var ContentRepository
     */
    private $contentRepository;

    /**
     * @var ContentTypeRepository
     */
    private $contentTypeRepository;

    /**
     * @var DashboardViewFactory
     */
    private $dashboardViewFactory;

    /**
     * @var Validation
     */
    private $validation;

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     */
    public function __construct(Request $request, Response $response, $args)
    {
        parent::__construct($request, $response, $args);

        $this->contentRepository = new ContentRepository(Container::getDatabase());
        $this->contentTypeRepository = new ContentTypeRepository(Container::getDatabase());
        $this->dashboardViewFactory = new DashboardViewFactory($this->contentTypeRepository);
        $this->validation = new Validation();
    }

    /**
     * @return string
     */
    protected function index()
    {
        return (new PageView($this->dashboardViewFactory->buildDashboardView(
            new OverviewView($this->contentRepository->findContentByType($this->args['type']), $this->args['type'])
        )))->parse();
    }

    /**
     * @return string
     */
    protected function add()
    {
        $inputData = null;

        $contentTypeData = $this->contentTypeRepository->findByCode($this->args['type']);
        if (isset($contentTypeData->getFields()->default) === false) {
            Container::getFlash()->addMessage(
                'notification',
                new NotificationView('danger', ['You need to configure this first!'])
            );
            return $this->response->withRedirect(Container::getRouter()->pathFor(
                'content-type-edit',
                ['id' => $contentTypeData->getId()]
            ));
        }

        if ($this->request->getMethod() === 'POST') {
            $inputData = $this->request->getParsedBody();

            $this->validation->setRules($this->getValidationRules($contentTypeData->getFields()));

            if ($this->validation->isValid($inputData) !== true) {
                return $this->returnFormView(null, $inputData, new NotificationView(
                    'danger',
                    $this->validation->getErrors()
                ));
            }

            $contentId = $this->contentRepository->create(
                $inputData,
                $this->args['type'],
                $this->adminLoggedIn->getId()
            );
            if ($contentId === false) {
                return $this->returnFormView(null, $inputData, new NotificationView(
                    'danger',
                    ['Unable to create page']
                ));
            }

            Container::getFlash()->addMessage(
                'notification',
                new NotificationView('success', ['A new page has been created'])
            );
            return $this->response->withRedirect(Container::getRouter()->pathFor('content-edit', ['id' => $contentId]));
        }

        return $this->returnFormView(null, $inputData);
    }

    /**
     * @return string
     */
    protected function edit()
    {
        $inputData = null;

        $content = $this->contentRepository->findById($this->args['id']);
        if ($content === false) {
            Container::getFlash()->addMessage(
                'notification',
                new NotificationView('danger', ['That page does not even exist yet!'])
            );
            return $this->response->withRedirect(Container::getRouter()->pathFor('index'));
        }

        if ($this->request->getMethod() === 'POST') {
            $inputData = $this->request->getParsedBody();

            $this->validation->setRules($this->getValidationRules($content->getContentType()->getFields()));

            if ($this->validation->isValid($inputData) !== true) {
                return $this->returnFormView($content, $inputData, new NotificationView(
                    'danger',
                    $this->validation->getErrors()
                ));
            }

            $status = $this->contentRepository->update($content, $inputData);
            if ($status === false) {
                return $this->returnFormView($content, $inputData, new NotificationView(
                    'danger',
                    ['Unable to update page']
                ));
            }

            return $this->returnFormView($content, $inputData, new NotificationView('success', ['Updated the page']));
        }

        return $this->returnFormView($content, $inputData);
    }

    /**
     * @return mixed
     */
    protected function sort()
    {
        $inputData = $this->request->getParsedBody();
        $this->contentRepository->sort($inputData['sortOrder']);

        return $this->renderSuccess($inputData['sortOrder']);
    }

    /**
     * @return mixed
     */
    protected function delete()
    {
        $content = $this->contentRepository->findById($this->args['id']);
        if ($content === false) {
            Container::getFlash()->addMessage(
                'notification',
                new NotificationView('danger', ['That page does not even exist yet!'])
            );
            return $this->response->withRedirect(Container::getRouter()->pathFor('index'));
        }

        $status = $this->contentRepository->delete($content);
        if ($status === false) {
            Container::getFlash()->addMessage(
                'notification',
                new NotificationView('danger', ['Could not remove the page.'])
            );
        } else {
            Container::getFlash()->addMessage(
                'notification',
                new NotificationView('success', ['The page has been removed.'])
            );
        }

        return $this->response->withRedirect(
            Container::getRouter()->pathFor('content', ['type' => $content->getContentType()->getCode()])
        );
    }

    /**
     * @param Content|null $content
     * @param string[]|null $inputData
     * @param View|null $notificationView
     * @return string
     */
    private function returnFormView(Content $content = null, $inputData = null, View $notificationView = null)
    {
        $contentType = ($content === null) ? $this->contentTypeRepository->findByCode($this->args['type']) : $content->getContentType();

        return (new PageView($this->dashboardViewFactory->buildDashboardView(
            new FormView($contentType, $content, $inputData, $notificationView)
        )))->parse();
    }

    /**
     * @param object $fields
     * @return array
     */
    private function getValidationRules($fields)
    {
        $validationRules = [
            'title' => Validator::stringType()->notEmpty()->setName('Title'),
            'sub_title' => Validator::stringType()->setName('Sub title'),
            'menu_title' => Validator::stringType()->notEmpty()->setName('Menu title'),
            'body' => Validator::stringType()->notEmpty()->setName('Content'),
            'preview' => Validator::stringType()->setName('Description')
        ];

        foreach ($validationRules as $key => $rule) {
            if (array_key_exists($key, $fields->default) === false) {
                unset($validationRules[$key]);
            }

            if ($key === 'body' && array_key_exists($key, $fields->default) === true) {
                $validationRules['files'] = Validator::stringType();
            }
        }

        if (isset($fields->extra) === true) {
            foreach ($fields->extra as $extraField) {
                if (isset($extraField->required) === true) {
                    $validationRules['extra_' . $extraField->name] = Validator::stringType()->notEmpty()
                        ->setName($extraField->label);
                } else {
                    $validationRules['extra_' . $extraField->name] = Validator::stringType()
                        ->setName($extraField->label);
                }
            }
        }

        $validationRules['status'] = Validator::intVal();

        return $validationRules;
    }
}