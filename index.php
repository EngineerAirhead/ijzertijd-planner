<?php
use \Psr\Http\Message\RequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require 'vendor/autoload.php';

try {
    $config = new \Airhead\Library\Framework\Config('Config/config.yaml');

    $app = new Slim\App(array('settings' => $config->get('settings')));

    // Fetch DI Container
    $container = $app->getContainer();

    // Register provider
    $container['flash'] = function () {
        return new \Slim\Flash\Messages();
    };

    /**********
     * Set base href for routing
     **********/
    $protocol = strtolower(substr($_SERVER['SERVER_PROTOCOL'], 0, 5)) == 'https://' ? 'https://' : 'http://';
    $path_parts = pathinfo($_SERVER['PHP_SELF']);
    $directory = ($path_parts['dirname'] == DIRECTORY_SEPARATOR) ? '' : $path_parts['dirname'];

    // Add to config
    $config->set('baseDir', dirname(__FILE__));
    $config->set('basePath', $protocol . $_SERVER['HTTP_HOST'] . $directory . '/');
    $config->set('baseUri', $protocol . $_SERVER['HTTP_HOST']);

    /**********
     * Create DI Container
     **********/
    $diContainer = new \Airhead\Library\Framework\Container($app, $config);

    /**********
     * Check for trailing slash and redirect to trailing if not found
     **********/
    $app->add(function (Request $request, Response $response, callable $next) {
        $uri = $request->getUri();
        $path = $uri->getPath();
        if ($path !== '/' && substr($path, -1) === '/') {
            $uri = $uri->withPath(substr($path, 0, -1));
            return $response->withRedirect((string)$uri, 301);
        }

        return $next($request, $response);
    });

    /**********
     * Create the routes
     **********/
    $routes = new \Airhead\Library\Framework\Route($app, 'Config/routing-cosmo.yaml');

    $app->run();
} catch (Exception $e) {
    die($e->getMessage());
}
