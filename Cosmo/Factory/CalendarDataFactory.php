<?php
/**
 * Created by PhpStorm.
 * User: jeroen
 * Date: 21-12-2016
 * Time: 10:12
 */

namespace Airhead\Cosmo\Factory;


use Airhead\Library\Repository\ModuleAgendaRepository;
use Airhead\Library\Repository\ModuleCalendarRepository;

class CalendarDataFactory
{
    /**
     * @var ModuleAgendaRepository
     */
    private $agendaRepository;

    /**
     * @var ModuleCalendarRepository
     */
    private $calendarRepository;

    /**
     * @param ModuleCalendarRepository $calendarRepository
     * @param ModuleAgendaRepository $agendaRepository
     */
    public function __construct(ModuleCalendarRepository $calendarRepository, ModuleAgendaRepository $agendaRepository)
    {
        $this->calendarRepository = $calendarRepository;
        $this->agendaRepository = $agendaRepository;
    }

    /**
     * @param int $year
     * @param int $month
     * @return \stdClass
     */
    public function buildCalendarDataObject($year, $month)
    {
        $startDate = $year.'-'.$month.'-01';
        $endDate = date('Y-m-t', strtotime($startDate));

        $calendarData = new \stdClass();
        $calendarData->datesBooked = [];
        $calendarData->datesEnabled = [];
        $calendarData->datesReserved = [];

        $datesReservedResultSet = $this->agendaRepository->findReservedBetweenDates($startDate, $endDate, false);
        if ($datesReservedResultSet !== false) {
            $calendarData->datesReserved = $datesReservedResultSet;
        }

        $datesBookedResultSet = $this->agendaRepository->findReservedBetweenDates($startDate, $endDate);
        if ($datesBookedResultSet !== false) {
            $calendarData->datesBooked = $datesBookedResultSet;
        }

        $datesEnabledResultSet = $this->calendarRepository->findBetweenDates($startDate, $endDate);
        if ($datesEnabledResultSet !== false) {
            $calendarData->datesEnabled = $datesEnabledResultSet;
        }

        return $calendarData;
    }
}