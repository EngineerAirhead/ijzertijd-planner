<?php
namespace Airhead\Cosmo\Factory;

use Airhead\Cosmo\Entity\DayEntity;
use Airhead\Cosmo\View\CalendarView;
use Airhead\Cosmo\View\NavigationView;

class CalendarFactory
{
    /**
     * @var string[]
     */
    private $dayLabels = array("MA","DI","WO","DO","VR","ZA","ZO");

    /**
     * @var int
     */
    private $currentYear;

    /**
     * @var int
     */
    private $currentMonth;

    /**
     * @var int
     */
    private $currentDay;

    /**
     * @var object
     */
    private $databaseDates;

    /**
     * @var int
     */
    private $daysInMonth;

    /**
     * Constructor
     * @param int $currentMonth
     * @param int $currentYear
     * @param object $databaseDates
     */
    public function __construct($currentMonth, $currentYear, $databaseDates)
    {
        $this->currentMonth = (int)$currentMonth;
        $this->currentYear = (int)$currentYear;
        $this->databaseDates = (object)$databaseDates;

        $this->daysInMonth = date('t',strtotime($this->currentYear.'-'.$this->currentMonth.'-01'));
    }

    /********************* PUBLIC *********************
     * @param bool $withNavigation
     * @return string
     */

    public function show($withNavigation = true) {

        $content = '';

        if ($withNavigation === true) {
            $content .= $this->_parseNavigation();
        }

        $content .= $this->_parseCalendar($this->getCalendartContainer());

        return $content;
    }

    /********************* PRIVATE **********************/

    /**
     * @return array
     */
    private function getCalendartContainer()
    {
        $weeksInMonth = ($this->daysInMonth%7==0?0:1) + intval($this->daysInMonth/7);
        $monthEndingDay= date('N',strtotime($this->currentYear.'-'.$this->currentMonth.'-'.$this->daysInMonth));
        $monthStartDay = date('N',strtotime($this->currentYear.'-'.$this->currentMonth.'-01'));

        if($monthEndingDay<$monthStartDay){
            $weeksInMonth++;
        }

        $calendarContainer = [];

        // Create weeks in a month
        for( $i=0; $i<$weeksInMonth; $i++ ){
            $calendarContainer[$i] = [];

            //Create days in a week
            for($j=1;$j<=7;$j++){
                $calendarContainer[$i][$j] = $this->_getDay($i*7+$j);
            }
        }

        return $calendarContainer;
    }

    /**
     * @param $cellNumber
     * @return DayEntity
     */
    private function _getDay($cellNumber)
    {
        if($this->currentDay==0){

            $firstDayOfTheWeek = date('N',strtotime($this->currentYear.'-'.$this->currentMonth.'-01'));

            if(intval($cellNumber) == intval($firstDayOfTheWeek)){
                $this->currentDay=1;
            }
        }

        $currentDate = null;
        $cellContent = null;

        if( ($this->currentDay!=0)&&($this->currentDay<=$this->daysInMonth) ){
            $currentDate = date('Y-m-d',strtotime($this->currentYear.'-'.$this->currentMonth.'-'.($this->currentDay)));
            $cellContent = $this->currentDay;

            $this->currentDay++;
        }

        return new DayEntity($currentDate, $cellContent);
    }

    /**
     * @return string
     */
    private function _parseNavigation() {
        return (new NavigationView())->parse();
    }

    /**
     * @param array<int|int|Day> $calendarContainer
     * @return string
     */
    private function _parseCalendar($calendarContainer)
    {
        return (new CalendarView($calendarContainer, $this->dayLabels, $this->databaseDates))->parse();
    }
}