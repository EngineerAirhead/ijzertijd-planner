<?php
namespace Airhead\Cosmo\View;

interface CalendarViewInterface
{
    /**
     * @return string
     */
    public function getCalendar();

    /**
     * @return string
     */
    public function getHeader();
}