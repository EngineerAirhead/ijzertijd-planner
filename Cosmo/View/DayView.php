<?php
namespace Airhead\Cosmo\View;

use Airhead\Cosmo\Entity\DayEntity;
use Airhead\Library\Framework\View;

class DayView extends View implements DayViewInterface
{
    /**
     * @var object
     */
    private $databaseDates;

    /**
     * @var DayEntity
     */
    private $day;

    /**
     * @param DayEntity $day
     * @param object $databaseDates
     */
    public function __construct(DayEntity $day, $databaseDates)
    {
        parent::__construct('Cosmo/Template/calendar-day');

        $this->day = $day;
        $this->databaseDates = (object)$databaseDates;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->day->getContent();
    }

    /**
     * @param string $format
     * @return string
     */
    public function getDate($format)
    {
        return date($format, strtotime($this->day->getDate()));
    }

    /**
     * @return bool
     */
    public function isAvailable()
    {
        return (
            strtotime($this->day->getDate()) > time() &&
            in_array($this->day->getDate(), $this->databaseDates->datesEnabled) === true &&
            in_array($this->day->getDate(), $this->databaseDates->datesBooked) === false &&
            in_array($this->day->getDate(), $this->databaseDates->datesReserved) === false
        );
    }

    /**
     * @return bool
     */
    public function isBooked()
    {
        return in_array($this->day->getDate(), $this->databaseDates->datesBooked) === true;
    }

    /**
     * @return bool
     */
    public function isEmptyDay()
    {
        return $this->day->isEmptyDay();
    }

    /**
     * @return bool
     */
    public function isReserved()
    {
        return in_array($this->day->getDate(), $this->databaseDates->datesReserved) === true;
    }

    /**
     * @return bool
     */
    public function isToday()
    {
        return $this->day->getDate() === date('Y-m-d');
    }
}