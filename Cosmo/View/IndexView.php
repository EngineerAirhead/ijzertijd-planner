<?php
namespace Airhead\Cosmo\View;

use Airhead\Cosmo\Factory\CalendarFactory;
use Airhead\Library\Framework\Container;
use Airhead\Library\Framework\View;

class IndexView extends View implements IndexViewInterface
{
    /**
     * @var CalendarFactory
     */
    private $calendarFactory;

    /**
     * @param CalendarFactory $calendarFactory
     */
    public function __construct(CalendarFactory $calendarFactory)
    {
        parent::__construct('Cosmo/Template/index');

        $this->calendarFactory = $calendarFactory;
    }

    /**
     * @return string
     */
    public function getCalendar()
    {
        return $this->calendarFactory->show();
    }

    /**
     * @return string
     */
    public function getCalendarAjaxUrl()
    {
        return Container::getRouter()->pathFor('ajax-calendar');
    }

    /**
     * @return string
     */
    public function getFormAjaxUrl()
    {
        return Container::getRouter()->pathFor('ajax-form');
    }
}