<?php
namespace Airhead\Cosmo\View;

interface NavigationViewInterface
{
    /**
     * @return array
     */
    public function getMonthSelectOptions();
}