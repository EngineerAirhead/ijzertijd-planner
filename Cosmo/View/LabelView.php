<?php
namespace Airhead\Cosmo\View;

use Airhead\Library\Framework\View;

class LabelView extends View implements LabelViewInterface
{
    /**
     * @var string[]
     */
    private $labels;

    /**
     * @param string[] $labels
     */
    public function __construct($labels)
    {
        parent::__construct('Cosmo/Template/labels');
        
        $this->labels = $labels;
    }

    /**
     * @return string[]
     */
    public function getLabels()
    {
        return $this->labels;
    }
}