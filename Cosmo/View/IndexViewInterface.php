<?php
namespace Airhead\Cosmo\View;

interface IndexViewInterface
{
    /**
     * @return string
     */
    public function getCalendar();

    /**
     * @return string
     */
    public function getCalendarAjaxUrl();

    /**
     * @return string
     */
    public function getFormAjaxUrl();
}