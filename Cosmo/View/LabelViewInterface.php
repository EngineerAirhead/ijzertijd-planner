<?php
namespace Airhead\Cosmo\View;

interface LabelViewInterface
{
    /**
     * @return string[]
     */
    public function getLabels();
}