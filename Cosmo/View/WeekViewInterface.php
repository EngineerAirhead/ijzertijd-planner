<?php
namespace Airhead\Cosmo\View;

interface WeekViewInterface
{
    /**
     * @return string
     */
    public function getWeek();
}