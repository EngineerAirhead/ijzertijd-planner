<?php
namespace Airhead\Cosmo\View\Email;

use Airhead\Library\Framework\View;

class EmailView extends View implements EmailViewInterface
{
    /**
     * @var View
     */
    private $body;

    /**
     * @var string
     */
    private $title;

    /**
     * @param string $title
     * @param View $body
     */
    public function __construct($title, View $body)
    {
        parent::__construct('Cosmo/Template/email/base');

        $this->title = (string)$title;
        $this->body = $body;
    }

    /**
     * @return string
     */
    public function getEmailBody()
    {
        return $this->body->parse();
    }

    /**
     * @return string
     */
    public function getEmailTitle()
    {
        return $this->title;
    }
}