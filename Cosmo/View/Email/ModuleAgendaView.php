<?php
namespace Airhead\Cosmo\View\Email;

use Airhead\Library\Framework\View;
use Airhead\Library\Model\ModuleAgenda;

class ModuleAgendaView extends View implements ModuleAgendaViewInterface
{
    /**
     * @var ModuleAgenda
     */
    private $moduleAgenda;

    /**
     * @param ModuleAgenda $moduleAgenda
     */
    public function __construct(ModuleAgenda $moduleAgenda)
    {
        parent::__construct('Cosmo/Template/email/module-agenda-add');

        $this->moduleAgenda = $moduleAgenda;
    }

    /**
     * @return string
     */
    public function getContactEmail()
    {
        return $this->moduleAgenda->getEmail();
    }

    /**
     * @return string
     */
    public function getContactName()
    {
        return $this->moduleAgenda->getName();
    }

    /**
     * @return string
     */
    public function getContactPhone()
    {
        return $this->moduleAgenda->getPhone();
    }

    /**
     * @return string
     */
    public function getContactSchool()
    {
        return $this->moduleAgenda->getSchool();
    }

    /**
     * @return string
     */
    public function getContactSchoolAddress()
    {
        return $this->moduleAgenda->getSchoolAddress();
    }

    /**
     * @return string
     */
    public function getContactSchoolEmail()
    {
        return $this->moduleAgenda->getSchoolEmail();
    }

    /**
     * @return string
     */
    public function getContactSchoolPhone()
    {
        return $this->moduleAgenda->getSchoolPhone();
    }

    /**
     * @return false|string
     */
    public function getDate()
    {
        return date('d-m-Y', strtotime($this->moduleAgenda->getDate()));
    }

    /**
     * @return string
     */
    public function getExtraInformation()
    {
        return $this->moduleAgenda->getInformation();
    }

    /**
     * @return int
     */
    public function getGroupAdults()
    {
        return $this->moduleAgenda->getNumAdults();
    }

    /**
     * @return int
     */
    public function getGroupChildren()
    {
        return $this->moduleAgenda->getNumKids();
    }

    /**
     * @return string
     */
    public function getGroupClass()
    {
        return $this->moduleAgenda->getClass();
    }

    /**
     * @return bool
     */
    public function getGroupConsume()
    {
        return $this->moduleAgenda->isConsume();
    }

    /**
     * @return string
     */
    public function getTimeArrive()
    {
        return $this->moduleAgenda->getTimeArrive();
    }

    /**
     * @return string
     */
    public function getTimeDepart()
    {
        return $this->moduleAgenda->getTimeDepart();
    }
}