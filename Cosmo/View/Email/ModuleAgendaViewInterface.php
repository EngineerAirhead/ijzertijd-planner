<?php
namespace Airhead\Cosmo\View\Email;

interface ModuleAgendaViewInterface
{
    /**
     * @return string
     */
    public function getContactEmail();

    /**
     * @return string
     */
    public function getContactName();

    /**
     * @return string
     */
    public function getContactPhone();

    /**
     * @return string
     */
    public function getContactSchool();

    /**
     * @return string
     */
    public function getContactSchoolAddress();

    /**
     * @return string
     */
    public function getContactSchoolEmail();

    /**
     * @return string
     */
    public function getContactSchoolPhone();

    /**
     * @return string
     */
    public function getDate();

    /**
     * @return string
     */
    public function getExtraInformation();

    /**
     * @return int
     */
    public function getGroupAdults();

    /**
     * @return int
     */
    public function getGroupChildren();

    /**
     * @return string
     */
    public function getGroupClass();

    /**
     * @return bool
     */
    public function getGroupConsume();

    /**
     * @return string
     */
    public function getTimeArrive();

    /**
     * @return string
     */
    public function getTimeDepart();
}