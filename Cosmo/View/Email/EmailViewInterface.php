<?php
namespace Airhead\Cosmo\View\Email;

interface EmailViewInterface
{
    /**
     * @return string
     */
    public function getEmailBody();

    /**
     * @return string
     */
    public function getEmailTitle();
}