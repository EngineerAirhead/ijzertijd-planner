<?php
namespace Airhead\Cosmo\View;

use Airhead\Library\Framework\Language\Month;
use Airhead\Library\Framework\View;

class NavigationView extends View implements NavigationViewInterface
{
    public function __construct()
    {
        parent::__construct('Cosmo/Template/navigation');
    }

    /**
     * @return array
     */
    public function getMonthSelectOptions()
    {
        $selectOptions = [];

        $month = date("m",time());
        $year = date("Y",time());

        for ($i=0; $i<=12; $i++) {
            $selectOptions[$year.'-'.$month] = Month::getMonthName($month) . ' ' . $year;

            $month++;
            if ($month > 12) {
                $month = 1;
                $year++;
            }
        }

        return $selectOptions;
    }
}