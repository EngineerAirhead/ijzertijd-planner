<?php
namespace Airhead\Cosmo\View;

use Airhead\Library\Framework\View;

class CalendarView extends View implements CalendarViewInterface
{
    /**
     * @var array<int|int|Day>
     */
    private $calendarContainer;

    /**
     * @var object
     */
    private $databaseDates;

    /**
     * @var string[]
     */
    private $labels;

    /**
     * @param array $calendarContainer
     * @param string[] $labels
     * @param object $databaseDates
     */
    public function __construct($calendarContainer, $labels, $databaseDates)
    {
        parent::__construct('Cosmo/Template/calendar');

        $this->calendarContainer = $calendarContainer;
        $this->labels = $labels;
        $this->databaseDates = (object)$databaseDates;
    }

    /**
     * @return string
     */
    public function getCalendar()
    {
        $content = '';
        foreach ($this->calendarContainer as $week => $days) {
            $content .= (new WeekView($days, $this->databaseDates))->parse();
        }

        return $content;
    }

    /**
     * @return string
     */
    public function getHeader()
    {
        return (new LabelView($this->labels))->parse();
    }
}