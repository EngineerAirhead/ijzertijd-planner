<?php
namespace Airhead\Cosmo\Entity;

class DayEntity
{
    /**
     * @var string
     */
    private $content;

    /**
     * @var string
     */
    private $date;

    /**
     * @param string $date
     * @param string $content
     */
    public function __construct($date, $content)
    {
        $this->date = (string)$date;
        $this->content = (string)$content;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @return string
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @return bool
     */
    public function isEmptyDay()
    {
        return $this->content === '';
    }
}