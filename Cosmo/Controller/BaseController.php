<?php
namespace Airhead\Cosmo\Controller;

use Airhead\Library\Framework\Container;
use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Route;

class BaseController
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var Response
     */
    protected $response;

    /**
     * @var array
     */
    protected $args;

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     */
    public function __construct(Request $request, Response $response, $args)
    {
        $this->request = $request;
        $this->response = $response;
        $this->args = $args;
    }

    /**
     * @param $method
     * @param $arguments
     * @return mixed
     */
    public function __call($method,$arguments) {
        if(method_exists($this, $method) === false) {
            throw new \BadMethodCallException('Method ' . $method . ' not found in Controller ' . get_class($this));
        }

        return call_user_func_array(array($this,$method),$arguments);
    }

    /**
     * @param mixed $feedback
     * @return mixed
     */
    public function renderSuccess($feedback)
    {
        return $this->response->withJson(
            ['status' => 'success', 'feedback' => $feedback],
            null,
            JSON_HEX_QUOT | JSON_HEX_TAG
        );
    }

    /**
     * @param string $feedback
     * @return mixed
     */
    public function renderError($feedback)
    {
        return $this->response->withJson(['status' => 'error', 'feedback' => $feedback]);
    }
}