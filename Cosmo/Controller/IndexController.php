<?php
namespace Airhead\Cosmo\Controller;

use Airhead\Cosmo\Factory\CalendarDataFactory;
use Airhead\Cosmo\Factory\CalendarFactory;
use Airhead\Cosmo\View\Email\EmailView;
use Airhead\Cosmo\View\Email\ModuleAgendaView;
use Airhead\Cosmo\View\IndexView;
use Airhead\Cosmo\View\NotificationView;
use Airhead\Library\Framework\Container;
use Airhead\Library\Framework\Validation;
use Airhead\Library\Repository\ModuleAgendaRepository;
use Airhead\Library\Repository\ModuleCalendarRepository;
use Airhead\Library\Repository\UserRepository;
use Respect\Validation\Validator;
use Slim\Http\Request;
use Slim\Http\Response;

class IndexController extends BaseController
{
    /**
     * @var CalendarDataFactory
     */
    private $calendarDataFactory;

    /**
     * @var ModuleAgendaRepository
     */
    private $moduleAgendaRepository;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var Validation
     */
    private $validation;

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     */
    public function __construct(Request $request, Response $response, $args)
    {
        parent::__construct($request, $response, $args);

        $this->moduleAgendaRepository = new ModuleAgendaRepository(Container::getDatabase());
        $this->calendarDataFactory = new CalendarDataFactory(
            new ModuleCalendarRepository(Container::getDatabase()),
            $this->moduleAgendaRepository
        );
        $this->userRepository = new UserRepository(Container::getDatabase());
        $this->validation = new Validation();
    }

    /**
     * @return string
     */
    protected function index()
    {
        $month = date("m",time());
        $year = date("Y",time());

        $calendarFactory = new CalendarFactory(
            $month,
            $year,
            $this->calendarDataFactory->buildCalendarDataObject($year, $month)
        );

        return (new IndexView($calendarFactory))->parse();
    }

    /**
     * @return array
     */
    protected function ajax()
    {
        $queryParams = $this->request->getQueryParams();


        $month = (isset($queryParams['month']) === true) ? $queryParams['month'] : date("m",time());
        $year = (isset($queryParams['year']) === true) ? $queryParams['year'] : date("Y",time());

        $calendarFactory = new CalendarFactory(
            $month,
            $year,
            $this->calendarDataFactory->buildCalendarDataObject($year, $month)
        );

        return $this->renderSuccess($calendarFactory->show(false));
    }

    /**
     * @return array
     */
    protected function form()
    {
        $inputData = $this->request->getParsedBody();

        $this->validation->setRules([
            'date' => Validator::stringType()->notEmpty()->setName('Datum'),
            'school' => Validator::stringType()->notEmpty()->setName('Naam school'),
            'address' => Validator::stringType()->notEmpty()->setName('Adres school'),
            'email_school' => Validator::email()->notEmpty()->setName('Email school'),
            'phone_school' => Validator::stringType()->notEmpty()->setName('Telefoon school'),
            'name' => Validator::stringType()->notEmpty()->setName('Naam contact persoon'),
            'email' => Validator::email()->notEmpty()->setName('Email adres'),
            'phone' => Validator::stringType()->notEmpty()->setName('Telefoon nummer'),
            'num_adults' => Validator::intVal()->notEmpty()->setName('Aantal volwassenen'),
            'num_kids' => Validator::intVal()->notEmpty()->setName('Aantal kinderen'),
            'time_arrive' => Validator::stringType()->notEmpty()->setName('Aankomst'),
            'time_depart' => Validator::stringType()->notEmpty()->setName('Vertrek'),
            'information' => Validator::stringType()->setName('Extra opmerkingen')
        ]);

        $this->validation->setCustomErrorMessage('date', 'notEmpty', 'Datum is leeg');
        $this->validation->setCustomErrorMessage('school', 'notEmpty', 'Naam school is leeg');
        $this->validation->setCustomErrorMessage('address', 'notEmpty', 'Adres school is leeg');
        $this->validation->setCustomErrorMessage('email_school', 'notEmpty', 'Email school is leeg');
        $this->validation->setCustomErrorMessage('email_school', 'email', 'Email school is ongeldig');
        $this->validation->setCustomErrorMessage('phone_school', 'notEmpty', 'Telefoon school is leeg');
        $this->validation->setCustomErrorMessage('name', 'notEmpty', 'Naam contact persoon is leeg');
        $this->validation->setCustomErrorMessage('email', 'email', 'Email adres is ongeldig');
        $this->validation->setCustomErrorMessage('phone', 'notEmpty', 'Telefoon nummer is leeg');
        $this->validation->setCustomErrorMessage('num_adults', 'notEmpty', 'Aantal volwassenen is leeg');
        $this->validation->setCustomErrorMessage('num_kids', 'notEmpty', 'Aantal kinderen is leeg');
        $this->validation->setCustomErrorMessage('num_adults', 'intVal', 'Aantal volwassenen moet een getal zijn');
        $this->validation->setCustomErrorMessage('num_kids', 'intVal', 'Aantal kinderen moet een getal zijn');
        $this->validation->setCustomErrorMessage('time_arrive', 'notEmpty', 'Aankomst is leeg');
        $this->validation->setCustomErrorMessage('time_depart', 'notEmpty', 'Vertrek is leeg');

        if ($this->validation->isValid($inputData) !== true) {
            return $this->renderError([
                'notification' => (new NotificationView('danger', $this->validation->getErrors()))->parse()
            ]);
        }

        // Check for duplicate
        $duplicate = $this->moduleAgendaRepository->findByDate(date('Y-m-d', strtotime($inputData['date'])));
        if ($duplicate !== false) {
            return $this->renderError([
                'notification' => (new NotificationView('danger', ['Voor deze datum hebben we al een inschrijving']))->parse()
            ]);
        }

        // Store information in database
        $moduleAgendaId = $this->moduleAgendaRepository->create($inputData);
        if ($moduleAgendaId === false) {
            return $this->renderError([
                'notification' => (new NotificationView('danger', ['We kunnen uw aanmelding op dit moment niet ontvangen, probeer het later nog een keer.']))->parse()
            ]);
        }

        $moduleAgenda = $this->moduleAgendaRepository->findById($moduleAgendaId);
        if ($moduleAgenda === false) {
            return $this->renderError([
                'notification' => (new NotificationView('danger', ['Dit kan niet kloppen...']))->parse()
            ]);
        }

        // Send email
        $mail = new \PHPMailer();

        $mail->setFrom('planning@ijzertijdboerderij.nl', 'Ijzertijd boerderij Dongen planning');
        $mail->addAddress($inputData['email'], $inputData['name']);

        // Collect all admins
        $admins = $this->userRepository->findAll();
        foreach ($admins as $admin) {
            $mail->addBCC($admin->getEmail(), $admin->getName());
        }
        $mail->addBCC('planning@ijzertijdboerderij.nl', 'Ijzertijd boerderij Dongen planning');

        $mail->isHTML(true);

        $mail->Subject = 'Uw school aanmelding bij ijzertijd boerderij Dongen';
        $mail->Body = (new EmailView(
            'Uw school aanmelding bij ijzertijd boerderij Dongen',
            new ModuleAgendaView($moduleAgenda)
        ))->parse();

        $returnMessage = 'Uw aanmelding is ontvangen.';
        if ($mail->send() === true) {
            $returnMessage .= ' Wij hebben een email gestuurd ter bevestiging en zullen zo spoedig mogelijk contact met u opnemen.';
        }

        // Show confirmation
        return $this->renderSuccess([
            'notification' => (new NotificationView('success', [$returnMessage]))->parse(),
        ]);
    }
}