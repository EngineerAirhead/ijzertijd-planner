# Da rules
###### You should know them by now

1. Timmy is what holds everything together
2. Vicky is the Content Management System
3. Wanda is the backbone for the backend (the smart stuff)
4. Cosmo is the backbone for the frontend (the dumb stuff)

Cosmo and Wanda each have their own library for all the Controllers, Views and other specific elements.
Both also have their own routing file in the config.
They are allowed to use the shared Library classes but are **NOT** allowed to extend or use each others classes! 