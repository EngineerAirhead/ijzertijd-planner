$(function(){
    function pageLoad(){
        $('.date-picker').datetimepicker();

        $('.selectpicker').selectpicker({
            format: false
        });

        $("#phone, #fax").mask("(999) 999-9999");
        $("#publish-time").mask("99:99");

        //teach select2 to accept data-attributes
        $(".chzn-select").each(function(){
            $(this).select2($(this).data());
        });
        $("#article-tags").select2({
            tags: ['photoshop', 'colors', 'plugins', 'themes', 'bike']
        });

        $(".wysisyg-editor").summernote({height: 400});

        $("#article-form").parsley();

        $('.widget').widgster();
    }

    pageLoad();
});